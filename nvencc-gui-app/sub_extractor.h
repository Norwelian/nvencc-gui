#ifndef SUB_EXTRACTOR_H
#define SUB_EXTRACTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QProcess>
#include <QCheckBox>
#include <QMessageBox>
#include <QLineEdit>
#include <QQueue>
#include <QTimer>

namespace Ui {
class Sub_Extractor;
}

class Sub_Extractor : public QDialog
{
    Q_OBJECT

public:
    explicit Sub_Extractor(QWidget *parent = 0, QString basedir = "");
    ~Sub_Extractor();

private:
    Ui::Sub_Extractor *ui;
    QString basedir;
    QTimer *timer;
    QProcess *proc;
    QList<QCheckBox*> *list_checkbox;
    QList<QLineEdit*> *list_suffix;
    QList<int> *list_track;
    QList<QLabel*> *list_label;
    QQueue<QStringList> *ext_queue;
    QString output_proc;
    bool ffmpeg_using;
    bool morethanonesub;
    bool closeonend;
    int last_track_id;

private slots:
    void select_file_slot();
    void ok_slot();
    void cancel_slot();
    void set_out_slot();
    void proc_read();
    void proc_starter(const QStringList& argz);
    void queue_tick();
    void extract_sub(int track_id, const QString& suffix);
    void view_info();
    void end_proc(int e);
    void toggle_buttons();
};

#endif // SUB_EXTRACTOR_H
