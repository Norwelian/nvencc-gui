#include "mainwindow.h"

Sub_Extractor::Sub_Extractor(QWidget *parent, QString basedir) :
    QDialog(parent),
    ui(new Ui::Sub_Extractor)
{
    ui->setupUi(this);
    ffmpeg_using = false;
    closeonend = false;
    last_track_id = -1;
    timer = new QTimer(this);
    proc = new QProcess(this);
    this->basedir = basedir;
    list_checkbox = new QList<QCheckBox*>();
    list_track = new QList<int>();
    list_suffix = new QList<QLineEdit*>();
    list_label = new QList<QLabel*>();
    ext_queue = new QQueue<QStringList>();
    connect(ui->explore, SIGNAL(clicked()), this, SLOT(select_file_slot()));
    connect(ui->cancel, SIGNAL(clicked()), this, SLOT(cancel_slot()));
    connect(ui->ok, SIGNAL(clicked()), this, SLOT(ok_slot()));
    connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
    connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));
    connect(ui->out_explore, SIGNAL(clicked()), this, SLOT(set_out_slot()));
}

Sub_Extractor::~Sub_Extractor()
{
    delete ui;
}

void Sub_Extractor::select_file_slot(){
    morethanonesub = false;
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open video to extract subs from"), basedir, "Video Files (*.mkv *.mp4 *.m4v)");
    if(!fileName.isEmpty()){
        ui->videoPath->setText(fileName);
        ui->videoOutput->setText(fileName.section("/", 0, -2));
        QDir dir(".");
        QString mediainfo_path;
        mediainfo_path = dir.absoluteFilePath("mediainfo/mediainfo.exe");
        mediainfo_path = dir.cleanPath(mediainfo_path);
        if(dir.exists(mediainfo_path)){ //Si existe mediainfo, lo usamos, sino, avisamos.
            QStringList arguments = QStringList() << "/C" << mediainfo_path << fileName;
            proc->start("cmd.exe", arguments);
        }
        ui->videoName->setText(MainWindow::edit_name_helper(fileName.remove(qobject_cast<MainWindow*>(parent())->get_basedir())));
    }
}

void Sub_Extractor::proc_read(){
    // WE WILL ASSUME AT THIS POINT THAT ALL ERROR OUTPUT WILL BE FFMPEG'S AND NVENC'S OUTPUT.
    // THE STANDARD OUTPUT IS THE OUTPUT OF MEDIAINFO MAINLY.
    QString tmpS = proc->readAllStandardOutput();
    QRegExp mediainfo_regex = QRegExp("Text #\\d+");
    QRegExp mediainfo_regex_one_sub = QRegExp("Text");
    QRegExp mediainfo_regex_id = QRegExp("ID +: (\\d+)");
    QRegExp mediainfo_regex_lang = QRegExp("Language");
    if(!tmpS.isEmpty()){
        int index_mediainfo = 0; //This captures the General thing in mediainfo output.
        while((index_mediainfo = tmpS.indexOf(mediainfo_regex, index_mediainfo+1)) != -1){
            int index_id_lang = tmpS.indexOf(mediainfo_regex_lang, index_mediainfo); //Here, we guess, if one track has language set, all tracks will have it.
            mediainfo_regex_id.indexIn(tmpS, index_mediainfo);
            QString text_n_id = mediainfo_regex_id.cap(1);
            QStringRef text_n_lang(&tmpS, tmpS.indexOf(":", index_id_lang)+2, 3); //we catch the first 3 letters of the lang
            auto* newSub = new QCheckBox();
            QLineEdit* newSuffix = new QLineEdit(((index_id_lang!=-1)?text_n_lang.toString().toLower():"t" + text_n_id), this);
            QLabel* newLabel = new QLabel("Suffix: ", this);
            newSub->setText("Track #" + text_n_id);
            newSub->setChecked(false);
            ui->gridLayout->addWidget(newSub, ui->gridLayout->rowCount(), 0);
            ui->gridLayout->addWidget(newLabel, ui->gridLayout->rowCount()-1, 1);
            ui->gridLayout->addWidget(newSuffix, ui->gridLayout->rowCount()-1, 2);
            list_checkbox->append(newSub);
            list_suffix->append(newSuffix);
            list_track->append(text_n_id.toInt()-1);
            list_label->append(newLabel);
            morethanonesub = true;
            //qDebug() << "FOUND TEXT. i: " << text_n.toString() << "id: " << text_n_id.toInt()-1;
        } if(((index_mediainfo = tmpS.indexOf(mediainfo_regex_one_sub)) != -1) && !morethanonesub){
            int index_id_lang = tmpS.indexOf(mediainfo_regex_lang, index_mediainfo); //Here, we guess, if one track has language set, all tracks will have it.
            mediainfo_regex_id.indexIn(tmpS, index_mediainfo);
            QString text_n_id = mediainfo_regex_id.cap(1);
            QStringRef text_n_lang(&tmpS, tmpS.indexOf(":", index_id_lang)+2, 3); //we catch the first 3 letters of the lang
            auto* newSub = new QCheckBox();
            QLineEdit* newSuffix = new QLineEdit(((index_id_lang!=-1)?text_n_lang.toString().toLower():"t" + text_n_id), this);
            QLabel* newLabel = new QLabel("Suffix: ", this);
            newSub->setText("Track #" + text_n_id);
            newSub->setChecked(false);
            ui->gridLayout->addWidget(newSub, ui->gridLayout->rowCount(), 0);
            ui->gridLayout->addWidget(newLabel, ui->gridLayout->rowCount()-1, 1);
            ui->gridLayout->addWidget(newSuffix, ui->gridLayout->rowCount()-1, 2);
            list_checkbox->append(newSub);
            list_suffix->append(newSuffix);
            list_track->append(text_n_id.toInt()-1);
            list_label->append(newLabel);
            //qDebug() << "FOUND TEXT. i: Text #1 id: " << text_n_id.toInt()-1;
        }
    }
}

void Sub_Extractor::end_proc(int e){
    if(!list_track->isEmpty() && !ui->ok->isEnabled()){
        ui->ok->setEnabled(true);
        disconnect(ui->explore, SIGNAL(clicked()), this, SLOT(select_file_slot()));
        ui->explore->setText("Info");
        connect(ui->explore, SIGNAL(clicked()), this, SLOT(view_info()));
    }
    if(closeonend){
        close();
        deleteLater();
    } else {
        proc = new QProcess(this);
        connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
        connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));
        if(ffmpeg_using){
            QMessageBox::information(this, tr("Subtitle extracted."), tr(qPrintable("Subtitle ID: " + QString::number(last_track_id) + " has ended extracting.")), QMessageBox::Ok);
            ffmpeg_using = false;
            last_track_id = -1;
        } if(e!=0)
            qDebug() << "[ ERROR ]" << "Process exited with error code: " + QString::number(e);
    }
}

void Sub_Extractor::ok_slot(){
    bool atleastone = false;
    for(int i = 0; i < list_checkbox->count(); i++){
        int sub_id = list_track->at(i);
        QString sub_suffix = list_suffix->at(i)->text();
        if(list_checkbox->at(i)->isChecked()){
            atleastone = true;
            extract_sub(sub_id, sub_suffix);
            //qDebug() << "Track ID: " << sub_id << "suffix: " << sub_suffix;
        }
    }
    if(atleastone)
        toggle_buttons();
    else
        QMessageBox::information(this, tr("No subtitles selected!"), tr("You didn't select any subtitle."), QMessageBox::Ok);
}

void Sub_Extractor::extract_sub(int track_id, const QString& suffix){
    QDir dir(".");
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    if(dir.exists(ffmpeg_path)){ //Si existe mediainfo, lo usamos, sino, avisamos.
        QStringList arguments = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << ui->videoPath->text() << "-map" << "0:"+QString::number(track_id) << ui->videoOutput->text() + "/" + ui->videoName->text() + "-" + suffix + ui->outExt->currentText();
        proc_starter(arguments);
        //qDebug() << arguments;
    } else
        QMessageBox::information(this, tr("Ffmpeg not found."), tr("Ffmpeg not found.\nIf you want to extract subtitles or use ffmpeg, please, follow the instructions in the git."), QMessageBox::Ok);
}

void Sub_Extractor::set_out_slot(){
    ui->videoOutput->setText(QFileDialog::getExistingDirectory(this , tr("Set output directory"), basedir, QFileDialog::ShowDirsOnly | QFileDialog::ReadOnly));
}

void Sub_Extractor::proc_starter(const QStringList& argz){
    ext_queue->enqueue(argz);
    if (!timer->isActive())
        timer->singleShot(1000, this, SLOT(queue_tick()) );
}

void Sub_Extractor::queue_tick(){
    if(proc->processId() == 0){ //if process is not running
        if(!ext_queue->isEmpty()){
            QStringList argz = ext_queue->dequeue();
            ffmpeg_using = true;
            last_track_id = argz.at(argz.size()-2).split(":").at(1).toInt();
            qDebug() << argz << last_track_id;
            proc->start("cmd.exe", argz);
        } else {
            toggle_buttons();
        }
    } else //if the process is already running, call the tick again
        timer->singleShot(1000, this, SLOT(queue_tick()));
}

void Sub_Extractor::view_info(){
    QDir dir(".");
    QString mediainfo_path;
    mediainfo_path = dir.absoluteFilePath("mediainfo/mediainfo.exe");
    mediainfo_path = dir.cleanPath(mediainfo_path);
    if(dir.exists(mediainfo_path)){ //Si existe mediainfo, lo usamos, sino, avisamos.
        QStringList arguments = QStringList() << "/C" << mediainfo_path << ui->videoPath->text();
        auto* m = qobject_cast<MainWindow*>(this->parent());

        m->mkvinfoscreen(arguments);
    }
}

void Sub_Extractor::toggle_buttons(){
    if(ui->ok->isEnabled()){
        ui->cancel->setEnabled(true);
        ui->ok->setEnabled(false);
    } else {
        ui->cancel->setEnabled(false);
        ui->ok->setEnabled(true);
    }
}

void Sub_Extractor::cancel_slot(){
    closeonend = true;
    ui->cancel->setEnabled(false);
    ui->ok->setEnabled(false);
}
