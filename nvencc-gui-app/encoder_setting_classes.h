#ifndef ENCODER_SETTING_CLASSES_H
#define ENCODER_SETTING_CLASSES_H

#include "QDataStream"
#include "QFile"

class FfmpegVideoSetting {
    public:
        int _0; //output_codec
        int _1; //max_rate
        int _2; //buff_size
        int _3; //CRF
        int _4; //preset
        int _5; //tune_selected
        bool _6; //bool_tune

        FfmpegVideoSetting(){}

        FfmpegVideoSetting(int output_codec, int max_rate, int buff_size, int CRF, int preset, int tune_selected, bool bool_tune)
        : _0(output_codec), _1(max_rate), _2(buff_size), _3(CRF), _4(preset), _5(tune_selected), _6(bool_tune)
        {
        }

        FfmpegVideoSetting(FfmpegVideoSetting *s)
        : _0(s->_0), _1(s->_1), _2(s->_2), _3(s->_3), _4(s->_4), _5(s->_5), _6(s->_6)
        {
        }
        friend QDataStream &operator>>(QDataStream &in, FfmpegVideoSetting &m);
        friend QDataStream &operator<<(QDataStream &out, const FfmpegVideoSetting &m);
};

class FfmpegAudioSetting {
    public:
        int _0; //output_codec
        int _1; //bitrate
        int _2; //audio_channel
        QString _3; //video_filter

        FfmpegAudioSetting(){}

        FfmpegAudioSetting(int output_codec, int bitrate, int audio_channel, QString video_filter)
        : _0(output_codec), _1(bitrate), _2(audio_channel), _3(video_filter)
        {
        }

        FfmpegAudioSetting(FfmpegAudioSetting *s)
        : _0(s->_0), _1(s->_1), _2(s->_2), _3(s->_3)
        {
        }
        friend QDataStream &operator>>(QDataStream &in, FfmpegAudioSetting &m);
        friend QDataStream &operator<<(QDataStream &out, const FfmpegAudioSetting &m);
};

class NvenccVideoSetting {
    public:
        int _0; //output_codec
        int _1; //bitrate
        int _2; //video_channel
        int _3; //FPS
        int _4; //aq_strength
        bool _5; //aq_temporal
        bool _6; //aq_spatial
        bool _7; //delete [video]___.mkv file when ended

        NvenccVideoSetting(){}

        NvenccVideoSetting(int output_codec, int bitrate, int video_channel, int FPS, int aq_strength, bool aq_temporal, bool aq_spatial, bool delete_temp_video)
        : _0(output_codec), _1(bitrate), _2(video_channel), _3(FPS), _4(aq_strength), _5(aq_temporal), _6(aq_spatial), _7(delete_temp_video)
        {
        }

        NvenccVideoSetting(NvenccVideoSetting *s)
        : _0(s->_0), _1(s->_1), _2(s->_2), _3(s->_3), _4(s->_4), _5(s->_5), _6(s->_6), _7(s->_7)
        {
        }
        friend QDataStream &operator>>(QDataStream &in, NvenccVideoSetting &m);
        friend QDataStream &operator<<(QDataStream &out, const NvenccVideoSetting &m);
};

class NvenccAudioSetting {
    public:
        int _0; //output_codec
        int _1; //bitrate
        int _2; //audio_channel

        NvenccAudioSetting(){}

        NvenccAudioSetting(int output_codec, int bitrate, int audio_channel)
        : _0(output_codec), _1(bitrate), _2(audio_channel)
        {
        }

        NvenccAudioSetting(NvenccAudioSetting *s)
        : _0(s->_0), _1(s->_1), _2(s->_2)
        {
        }
        friend QDataStream &operator>>(QDataStream &in, NvenccAudioSetting &m);
        friend QDataStream &operator<<(QDataStream &out, const NvenccAudioSetting &m);
};

class OtherSetting {
    public:
        QString _0; //out_prefix
        int _1; //first_episode
        bool _2; //sub_extraction
        int _3; //sub_track
        QString _4; //sub_suffix
        bool _5; //delete_on_finish

        OtherSetting(){}

        OtherSetting(QString out_prefix, int first_episode, bool sub_extraction, int sub_track, QString sub_suffix, bool delete_on_finish)
        : _0(out_prefix), _1(first_episode), _2(sub_extraction), _3(sub_track), _4(sub_suffix), _5(delete_on_finish)
        {
        }

        OtherSetting(OtherSetting *s)
        : _0(s->_0), _1(s->_1), _2(s->_2), _3(s->_3), _4(s->_4), _5(s->_5)
        {
        }
        friend QDataStream &operator>>(QDataStream &in, OtherSetting &m);
        friend QDataStream &operator<<(QDataStream &out, const OtherSetting &m);
};

class Setting {
    public:
        FfmpegVideoSetting ffmpeg_video_setting;
        FfmpegAudioSetting ffmpeg_audio_setting;
        NvenccVideoSetting nvencc_video_setting;
        NvenccAudioSetting nvencc_audio_setting;
        OtherSetting other_setting;
        int mode;

        Setting(){}

        Setting(FfmpegVideoSetting ffmpeg_video_setting, FfmpegAudioSetting ffmpeg_audio_setting, NvenccVideoSetting nvencc_video_setting, NvenccAudioSetting nvencc_audio_setting, OtherSetting other_setting, int mode)
        : ffmpeg_video_setting(ffmpeg_video_setting), ffmpeg_audio_setting(ffmpeg_audio_setting), nvencc_video_setting(nvencc_video_setting), nvencc_audio_setting(nvencc_audio_setting), other_setting(other_setting), mode(mode)
        {
        }

        Setting(Setting *s)
        : ffmpeg_video_setting(s->ffmpeg_video_setting), ffmpeg_audio_setting(s->ffmpeg_audio_setting), nvencc_video_setting(s->nvencc_video_setting), nvencc_audio_setting(s->nvencc_audio_setting), other_setting(s->other_setting), mode(s->mode)
        {
        }
        friend QDataStream &operator>>(QDataStream &in, Setting &m);
        friend QDataStream &operator<<(QDataStream &out, const Setting &m);
};

Q_DECLARE_METATYPE(Setting);

#endif // ENCODER_SETTING_CLASSES_H
