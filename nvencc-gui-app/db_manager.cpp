#include "db_manager.h"
#include "ui_db_manager.h"

DB_manager::DB_manager(QWidget *parent, const QString& db_path) :
    QDialog(parent),
    ui(new Ui::DB_manager)
{
    ui->setupUi(this);

    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(load_db()));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(save_changes_slot()));
    connect(ui->tableWidget, SIGNAL(cellClicked(int, int)), this, SLOT(select_row_slot(int, int)));

    connect(ui->lineEdit, SIGNAL(textEdited(QString)), this, SLOT(edited_slot()));
    connect(ui->lineEdit_2, SIGNAL(textEdited(QString)), this, SLOT(edited_slot()));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(edited_slot()));

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(db_path);
    if(db.open()){
        //qDebug() << "CONNECTED!";
        ui->pushButton_3->setEnabled(true);
    } else {
        //qDebug() << db.lastError();
        close();
        deleteLater();
    }
}

void DB_manager::load_db(){
    ui->tableWidget->setRowCount(0);
    QSqlQuery query("SELECT * FROM jobs");
    while(query.next()){
        int id = query.record().indexOf("id");
        int id_name = query.record().indexOf("name");
        int id_fullpath = query.record().indexOf("full_path");
        int id_type = query.record().indexOf("type");

        QString id_ = query.value(id).toString();
        QString name = query.value(id_name).toString();
        QString full_path = query.value(id_fullpath).toString();
        QString type = query.value(id_type).toString();

        auto *it_id = new QTableWidgetItem(id_);
        auto *it_name = new QTableWidgetItem(name);
        auto *it_full_path = new QTableWidgetItem(full_path);
        auto *it_type = new QTableWidgetItem(type);

        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, it_id);
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, it_name);
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 2, it_full_path);
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, it_type);

        //qDebug() << "-------\nID: " << id_ << "\nName: " << name << "\nFull path: " << full_path << "\nType:" << type << "\n-------";
    }
    ui->tableWidget->setCurrentCell(0,0);
    select_row_slot(0,0);
}

void DB_manager::select_row_slot(int row, int  /*col*/){
    ui->label_2->setText(ui->tableWidget->item(row, 0)->text());
    ui->lineEdit->setText(ui->tableWidget->item(row, 1)->text());
    ui->lineEdit_2->setText(ui->tableWidget->item(row, 2)->text());
    if(ui->tableWidget->item(row, 3)->text() == "F") {
        ui->comboBox->setCurrentIndex(0);
    } else if(ui->tableWidget->item(row, 3)->text() == "D") {
        ui->comboBox->setCurrentIndex(1);
    }
}

void DB_manager::edited_slot(){
    if(!ui->pushButton->isEnabled()){
        ui->pushButton->setEnabled(true);
    }
}

void DB_manager::save_changes_slot(){
    QSqlQuery query;
    query.prepare("UPDATE jobs SET name=(:newname), full_path=(:newpath), type=(:newtype) WHERE id=(:id);");
    query.bindValue(":newname", ui->lineEdit->text());
    query.bindValue(":newpath", ui->lineEdit_2->text());
    query.bindValue(":newtype", (ui->comboBox->currentText()=="File"?"F":"D"));
    query.bindValue(":id", ui->label_2->text());
    bool succ = query.exec();
    if(succ){
        ui->pushButton->setEnabled(false);
        load_db();
    } //else
        //qDebug() << "ERROR: " << query.lastError();
    close();
    deleteLater();
}

DB_manager::~DB_manager()
{
    delete ui;
}
