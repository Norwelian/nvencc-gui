# nvencc-gui.exe shared needs:
#### libgcc_s_seh-1.dll:

This is my case, in your case you'll need the libgcc you used to compile `nvencc-gui.exe`. If you used `mingw64`, it's in the `bin/` folder. Of course, you *won't* need it if you compile it statically.

#### libstdc++-6.dll:

This is my case, in your case you'll need the libstdc++ you used to compile `nvencc-gui.exe`. If you used `mingw64`, it's in the `bin/` folder. Of course, you *won't* need it if you compile it statically.

#### Qt5Core.dll

#### Qt5Gui.dll

#### Qt5Widgets.dll

#### platform/qtwindows.dll

In my case, because I use Windows.