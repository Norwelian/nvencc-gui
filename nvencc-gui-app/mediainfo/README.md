# MediaInfo

If you want to see the media information of the files/directories by double-clicking them on the job list, build or download `mediainfo.exe`, and put it in this directory, with the name as you see back in the sentence, without quotation marks.

I know there are a lot of mediainfo tools out there, but as long as it spews the content on stdout/stderr, it should work.

The one I personally use is MediaInfo from [MediaInfo](https://github.com/MediaArea/MediaInfo). 

This folder goes in the directory where you will be running the program, as the others.