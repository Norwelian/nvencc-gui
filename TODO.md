# NVEncC: [DONE]

# Ffmpeg: [DONE]

# Waifu2x: [IN-PROGRESS]
- Extract the original audio
- Extract the original frames to a folder
- For chunk in chunks_of_n_frames:
	- Process the chunk of n frames with waifu2x
	- Remake the corresponding chunk of the video with the new frames
	- Delete the processed images (optional: the images represent high disk usage)
- Merge all the chunks into the upscaled video
- Merge the original audio with the new video

# Auto subtitle extraction: [DONE]
- Add option to autoextract more than one sub.

# Tools>Subtitle extractor: [DONE]

# MediaInfo: [DONE]
- Custom parse for mediainfo on directories, to avoid WALL OF TEXT.

# GUI:
- Per-item encoding settings, changing when selecting different rows in the job queue. Initialized with default settings [DONE].

- Save button for queues

- Complete statustips

# PRESETS: [DONE]

# In the far future:
- More themes (even though the default one now looks amazin imho)

- Refactoring and comments for new devs

- Auto upload things to Norwix checbox toggleable dev build (disabled on other PC than mine)