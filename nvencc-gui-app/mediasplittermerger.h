#ifndef MEDIASPLITTERMERGER_H
#define MEDIASPLITTERMERGER_H

#include <QDialog>
#include <QStringList>
#include <qfileselectionwidget.h>
#include <qprocessqueue.h>

namespace Ui {
class MediaSplitterMerger;
}

class MediaSplitterMerger : public QDialog
{
    Q_OBJECT

public:
    explicit MediaSplitterMerger(QWidget *parent = nullptr, QString basedir = "");
    ~MediaSplitterMerger();
    void split_file(const QString& input, const QString& output_v, const QString& output_a);
    void merge_file(const QString& input_v, const QString& input_a, const QString& output);
    void merge_file_bulk(const QString& input_v_regex, const QString& input_a_regex, const QString& output_regex);

private:
    Ui::MediaSplitterMerger *ui;
    QList<QFileSelectionWidget*> qfileselectionwidget_list;
    QProcessQueue *proc_queue;
    QString basedir;

    void init_qfsw();

private slots:
    void split_btn_slot();
    void merge_btn_slot();
    void merge_bulk_btn_slot();
    void close_btn_slot();
    void tasks_end_slot();
    void input_va_slot(const QString& file_path);
    void input_v_slot(const QString& file_path);
    void aux_input_v_autoselect_audio(QString *new_file_out, QString str_orig, QString str_new);
};

#endif // MEDIASPLITTERMERGER_H
