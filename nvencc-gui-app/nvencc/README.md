# What is this folder
Download NvEncC by rigaya from his [github](https://github.com/rigaya/NVEnc).

Extract it, and copy all the contents of `NVEnc_x.yy\NVEncC\x64` here.

Then, put this folder where you will be running your compiled program.

# NVEncC errors or issues

If NVEncC has any error on itself when transcoding, post the issue in NVEncC's github.

**ONLY** post the issue in this project if it's a **GUI-related bug**.