#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             1,0,0,0
#define VER_FILEVERSION_STR         "1.0.0.0\0"

#define VER_PRODUCTVERSION          1,0,0,0
#define VER_PRODUCTVERSION_STR      "1.0\0"

#define VER_COMPANYNAME_STR         "Norwelian"
#define VER_FILEDESCRIPTION_STR     "NVEncC GUI"
#define VER_INTERNALNAME_STR        "NVEncC GUI"
#define VER_LEGALCOPYRIGHT_STR      "Copyright 2017-2020 @Norwelian"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "nvencc-gui-app.exe"
#define VER_PRODUCTNAME_STR         "NvEncC GUI"

#define VER_COMPANYDOMAIN_STR       "norwix.es"

#endif // VERSION_H
