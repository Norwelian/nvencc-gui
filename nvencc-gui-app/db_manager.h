#ifndef DB_MANAGER_H
#define DB_MANAGER_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>

namespace Ui {
class DB_manager;
}

class DB_manager : public QDialog
{
    Q_OBJECT

public:
    explicit DB_manager(QWidget *parent = nullptr, const QString& db_path = "");
    ~DB_manager();

private:
    Ui::DB_manager *ui;
    QSqlDatabase db;

private slots:
    void load_db();
    void select_row_slot(int row, int col);
    void edited_slot();
    void save_changes_slot();
};

#endif // DB_MANAGER_H
