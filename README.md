# NVEncC-GUI
This project was born from pure laziness when transcoding series and movies.

It started being a simple GUI to automate trancoding by NVEncC, but now it's a full program to manage and automate all video and audio jobs that you may need to do (made in Qt and C++) that uses a freshly-compiled version of ffmpeg (if possible) and [NVEncC by rigaya](https://github.com/rigaya/NVEnc) to automate transcoding of entire folders and/or single files (and extraction of subtitles).

NVEncC is used to hardware-accelerate the transcoding using Nvidia's NVENC/NVDEC.

For more instructions, to check if it's working in your PC, see if your graphics card is compatible, or other questions regarding NVEncC, please check [rigaya's github](https://github.com/rigaya/NVEnc).

Ffmpeg is used for everything else, including the things that NVEncC can't transcode.

# Progress:
- NVEncC is working with files AND directories now!

- Gonna find bugs as soon as I end the subtitle extraction part.

- Subtitle extraction now works!

- Presets now work!

- Ffmpeg now works too!

- Log system (of transcoding jobs) added (default on).

- `Tools>Subtitle extractor` functional!

- Encoding queue added, to see the job queue.

- Media splitter / merger tool working!!

- Drag 'n' drop working for video files and directories!

- The per-item encoding settings now work!!

- -> Next thing: Save and Load the job queue and all encoding settings they have.

- -> Now working on adding support for automatic Waifu2x video up/down-scaling (NOT WORKING RIGHT NOW). Maybe change the name of the project at this point?

# Screenshots:
  >
  ![](https://norpushy.ddns.net/perma/nvencc-gui_1.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_2.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_3.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_7.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_4.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_8.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_5.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_6.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_9.png)
  ![](https://norpushy.ddns.net/perma/nvencc-gui_10.png)

# Build:
I use Qt Creator in Windows 7 x64, with a custom compiling kit consisting in mingw64 (*gcc/g++-10.2.0-x86_64* right now) and a custom-compiled static Qt (*qt-5.15.1-x86_64*, the last version).

If you are having trouble, in google you may find what you seek (as I did in the moment).

Appart from that, you'll need to compile or download: [NVEncC by rigaya](https://github.com/rigaya/NVEnc), [ffmpeg](https://github.com/FFmpeg/FFmpeg) and [MediaInfo](https://github.com/MediaArea/MediaInfo).

I've hand-checked and listed the dependencies of the program (my GUI) in `DLL-DEPENDENCIES.md`, but I'm building it statically right now.

Look at the respective READMEs for more information.

# Release:
If you REALLY need it, you can ask me in my e-mail (penrodal [at] hotmail [dot] com) for a packaged setup (I build one for me, because for licensing things, I cannot redistribute the third-party tools as ffmpeg that I use), but I really *REALLY* suggest you to compile it all for yourself!

# License:
I have no idea of that stuff for now, but feel free to mail me, either to criticise, give an idea, or whatever.

I suppose I will MIT-license this, as it depends in a few tools that are MIT-licensed to work (not really depends directly, and the code is not directly here, but it's needed to function), and is an open source license.

# Contact:
Devs:

* Norwelian (penrodal [at] hotmail [dot] com) - [norpushy.ddns.net](https://norpushy.ddns.net/)

For now it's only me!

Good luck!
