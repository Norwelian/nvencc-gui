#ifndef MKVI_H
#define MKVI_H

#include <QDialog>
#include <QProcess>
#include <QScrollBar>

namespace Ui {
class MKVi;
}

class MKVi : public QDialog
{
    Q_OBJECT

public:
    explicit MKVi(QWidget *parent = nullptr, QProcess* proc = nullptr);
    ~MKVi() override;
    void showios(const QString& title, int sw);
    Ui::MKVi *ui;

private:
    QProcess* parent_proc;
    int max_slider;

private slots:
    void exitt();
    void slider_max();
    void reject() override;
};

#endif // MKVI_H
