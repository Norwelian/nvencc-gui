#-------------------------------------------------
#
# Project created by QtCreator 2018-01-27T00:40:54
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += static

TARGET = nvencc-gui-app
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    encoder_setting_classes.cpp \
        main.cpp \
        mainwindow.cpp \
        mkvi.cpp \
        sub_extractor.cpp \
        db_manager.cpp \
    mediasplittermerger.cpp

HEADERS += \
    encoder_setting_classes.h \
        mainwindow.h \
        mkvi.h \
        sub_extractor.h \
        db_manager.h \
    mediasplittermerger.h

FORMS += \
        mainwindow.ui \
        mkvi.ui \
        mkvi.ui \
        sub_extractor.ui \
        db_manager.ui \
    mediasplittermerger.ui

RC_FILE = \
        style/resources.rc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build/NorLib/release/ -lNorLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build/debug/NorLib/debug/ -lNorLib
else:unix: LIBS += -L$$PWD/../build/NorLib/ -lNorLib

INCLUDEPATH += $$PWD/../NorLib
DEPENDPATH += $$PWD/../NorLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build/NorLib/release/libNorLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build/debug/NorLib/debug/libNorLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build/NorLib/release/NorLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build/debug/NorLib/debug/NorLib.lib
else:unix: PRE_TARGETDEPS += $$PWD/../build/NorLib/libNorLib.a
