#include "mediasplittermerger.h"
#include "ui_mediasplittermerger.h"

MediaSplitterMerger::MediaSplitterMerger(QWidget *parent, QString basedir) :
    QDialog(parent),
    ui(new Ui::MediaSplitterMerger)
{
    ui->setupUi(this);

    proc_queue = new QProcessQueue();
    this->basedir = basedir;

    connect(ui->button_close, SIGNAL(clicked()), this, SLOT(close_btn_slot()));
    connect(ui->button_split, SIGNAL(clicked()), this, SLOT(split_btn_slot()));
    connect(ui->button_merge, SIGNAL(clicked()), this, SLOT(merge_btn_slot()));
    connect(ui->button_merge_2, SIGNAL(clicked()), this, SLOT(merge_bulk_btn_slot()));

    init_qfsw();
}
MediaSplitterMerger::~MediaSplitterMerger()
{
    delete ui;
}

void MediaSplitterMerger::split_file(const QString& input, const QString& output_v, const QString& output_a){
    QDir dir(".");
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    if(dir.exists(ffmpeg_path)){ //Si existe ffmpeg, lo usamos, sino, avisamos.
        QStringList argz_v = QStringList() << ffmpeg_path << "-y" << "-i" << input <<
                                              "-c:v" << "copy" << "-an" << "-map" << "0:0" << output_v;
        QStringList argz_a = QStringList() << ffmpeg_path << "-y" << "-i" << input <<
                                              "-c:a" << "copy" << "-vn" << "-map" << "0:0" << output_a;
        proc_queue->queue_add(argz_v);
        proc_queue->queue_add_start(argz_a);
    }
}
void MediaSplitterMerger::merge_file(const QString& input_v, const QString& input_a, const QString& output){
    QDir dir(".");
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    if(dir.exists(ffmpeg_path)){ //Si existe ffmpeg, lo usamos, sino, avisamos.
        QStringList argz = QStringList() << ffmpeg_path << "-y" <<
                                            "-i" << input_v <<
                                            "-i" << input_a <<
                                            "-c:v" << "copy" << "-c:a" << "copy" << "-map" << "0:0" << "-map" << "1:0" << output;
        proc_queue->queue_add_start(argz);
    }
}

void MediaSplitterMerger::merge_file_bulk(const QString& input_v_regex, const QString& input_a_regex, const QString& output_regex){
    QDir dir(".");
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    QRegExp episode_regex = QRegExp("(E\\d+)");
    if(dir.exists(ffmpeg_path)){ //Si existe ffmpeg, lo usamos, sino, avisamos.
        int input_v_index = episode_regex.indexIn(input_v_regex);
        int first_ep = episode_regex.cap(1).remove("E").toInt();
        int input_a_index = episode_regex.indexIn(input_a_regex);
        int output_index = episode_regex.indexIn(output_regex);
        QString input_v_prefix = QString(input_v_regex).replace("E"+QString::number(first_ep), "");
        QString input_a_prefix = QString(input_a_regex).replace("E"+QString::number(first_ep), "");
        QString output_prefix = QString(output_regex).replace("E"+QString::number(first_ep), "");
        if(input_v_index != -1 && input_a_index != -1 && output_index != -1)
            for(int i = 0; i < ui->spin_episodes->value(); i++){
                int temp_suma = first_ep + i;
                QStringList argz = QStringList() << ffmpeg_path << "-y" <<
                                                    "-i" << QString(input_v_prefix).insert(input_v_index, ((temp_suma<10)?"E0"+QString::number(temp_suma):"E"+QString::number(temp_suma))) <<
                                                    "-i" << QString(input_a_prefix).insert(input_a_index, ((temp_suma<10)?"E0"+QString::number(temp_suma):"E"+QString::number(temp_suma))) <<
                                                    "-c:v" << "copy" << "-c:a" << "copy" << "-map" << "0:0" << "-map" << "1:0" << QString(output_prefix).insert(output_index, ((temp_suma<10)?"E0"+QString::number(temp_suma):"E"+QString::number(temp_suma)));
                proc_queue->queue_add_start(argz);
            }
    }
}

void MediaSplitterMerger::split_btn_slot(){
    split_file(qobject_cast<QFileSelectionWidget*>(ui->qfileselection_split_input)->path(), qobject_cast<QFileSelectionWidget*>(ui->qfileselection_split_output_v)->path(), qobject_cast<QFileSelectionWidget*>(ui->qfileselection_split_output_a)->path());
    ui->button_close->setEnabled(false);
    connect(proc_queue, SIGNAL(all_proc_end()), this, SLOT(tasks_end_slot()));
}

void MediaSplitterMerger::merge_btn_slot(){
    merge_file(qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_input_v)->path(), qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_input_a)->path(), qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_output)->path());
    ui->button_close->setEnabled(false);
    connect(proc_queue, SIGNAL(all_proc_end()), this, SLOT(tasks_end_slot()));
}

void MediaSplitterMerger::merge_bulk_btn_slot(){
    merge_file_bulk(qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_input_v_2)->path(), qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_input_a_2)->path(), qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_output_2)->path());
    ui->button_close->setEnabled(false);
    connect(proc_queue, SIGNAL(all_proc_end()), this, SLOT(tasks_end_slot()));
}

void MediaSplitterMerger::close_btn_slot(){
    close();
    deleteLater();
}

void MediaSplitterMerger::tasks_end_slot(){
    ui->button_close->setEnabled(true);
}

void MediaSplitterMerger::input_va_slot(const QString& file_path){
    QString new_file_aud = "[audio]" + file_path;
    QString new_file_vid = "[video]" + file_path;
    auto output_a = qobject_cast<QFileSelectionWidget*>(ui->qfileselection_split_output_a);
    auto output_v = qobject_cast<QFileSelectionWidget*>(ui->qfileselection_split_output_v);
    output_a->select_file(new_file_aud, false);
    output_v->select_file(new_file_vid, false);
}
void MediaSplitterMerger::input_v_slot(const QString& file_path){
    QString new_file_out = file_path;
    if(file_path.contains("-es."))
        aux_input_v_autoselect_audio(&new_file_out, "-es.", "-en.");
    else if(file_path.contains("-en."))
        aux_input_v_autoselect_audio(&new_file_out, "-en.", "-es.");
    else if(file_path.contains("-jp."))
        aux_input_v_autoselect_audio(&new_file_out, "-jp.", "-en.");
    else
        aux_input_v_autoselect_audio(&new_file_out, "", "");

    auto input_a = qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_input_a);
    auto output = qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_output);

    if(!input_a->selected()){
        QString tmp = new_file_out;
        tmp.insert(tmp.lastIndexOf("."), "-en");
        aux_input_v_autoselect_audio(&tmp, "", "");
        if(!input_a->selected())
            new_file_out.append("_merg.mp4");
        else
            new_file_out.insert(new_file_out.lastIndexOf("."), "-en");

    }
    output->select_file(new_file_out, false);
}

void MediaSplitterMerger::aux_input_v_autoselect_audio(QString *new_file_out, QString str_orig, QString str_new){
    if(!str_orig.isEmpty())
        new_file_out->replace(str_orig, str_new);
    if(new_file_out->contains("[video]"))
        new_file_out->replace("[video]", "[audio]");
    else
        new_file_out->insert(new_file_out->lastIndexOf("/")+1, "[audio]");
    auto input_a = qobject_cast<QFileSelectionWidget*>(ui->qfileselection_merge_input_a);
    input_a->select_file(*new_file_out, true);
    new_file_out->remove("[audio]");
}

void MediaSplitterMerger::init_qfsw(){
    QFileSelectionWidget *aux_qfsw;
    QString video_filter = "Video Files (*.mkv *.mp4);;All Files (*.*)";
    QString audio_filter = "Audio Files (*.mkv *.mp3 *.aac);;All Files (*.*)";
    // SPLIT
    // INPUT V+A
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_split_input);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(true); //input
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(video_filter);
    connect(aux_qfsw, SIGNAL(selected_file(QString)), this, SLOT(input_va_slot(QString)));
    ui->qfileselection_split_input = aux_qfsw;
    // OUTPUT V
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_split_output_v);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(false); //output
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(video_filter);
    ui->qfileselection_split_output_v = aux_qfsw;
    // OUTPUT A
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_split_output_a);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(false); //output
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(audio_filter);
    ui->qfileselection_split_output_a = aux_qfsw;
    // MERGE
    // INPUT V
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_merge_input_v);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(true); //input
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(video_filter);
    connect(aux_qfsw, SIGNAL(selected_file(QString)), this, SLOT(input_v_slot(QString)));
    ui->qfileselection_merge_input_v = aux_qfsw;
    // INPUT A
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_merge_input_a);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(true); //input
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(audio_filter);
    ui->qfileselection_merge_input_a = aux_qfsw;
    // OUTPUT V+A
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_merge_output);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(false); //output
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(video_filter);
    ui->qfileselection_merge_output = aux_qfsw;
    // MERGE BULK
    // INPUT V
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_merge_input_v_2);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(true); //input
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(video_filter);
    connect(aux_qfsw, SIGNAL(selected_file(QString)), this, SLOT(input_v_slot(QString)));
    ui->qfileselection_merge_input_v = aux_qfsw;
    // INPUT A
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_merge_input_a_2);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(true); //input
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(audio_filter);
    ui->qfileselection_merge_input_a = aux_qfsw;
    // OUTPUT V+A
    aux_qfsw = new QFileSelectionWidget(ui->qfileselection_merge_output_2);
    qfileselectionwidget_list.append(aux_qfsw);
    aux_qfsw->conf_set_show_path_mode(false);
    aux_qfsw->conf_set_editable_path_state(false);
    aux_qfsw->conf_set_file_open_save_mode(false); //output
    aux_qfsw->conf_set_basepath(basedir);
    aux_qfsw->conf_set_browse_extension_filter(video_filter);
    ui->qfileselection_merge_output = aux_qfsw;
}
