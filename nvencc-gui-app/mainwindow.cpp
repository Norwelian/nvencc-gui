#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setStyleSheet(NorLib::style_dark());

    pathList = new QList<QStringList>(); //paths in the form: [ ["path1", {F/D}], ... ]

    QDir tt(".");
    basedir = "";
    if(QDir("E:/Downloads/recoding/").exists())
        basedir = "E:/Downloads/recoding/";
    while(basedir=="")
        change_base_dir_slot();

    log_enabled = 0;
    log_file = tt.cleanPath(tt.absolutePath()) + "/nvencc-gui_history.log";

    error_flag = false;

    media_splitter_merger = new MediaSplitterMerger(this, basedir);
    proc = new QProcess(this);
    process_paused = false;

    //We save the tab list
    tabList = new QList<QWidget*>();
    for(int j = 0; j < ui->settings_tab->count(); j++){
        tabList->append(ui->settings_tab->widget(j));
        //qDebug() << tabList->last();
    }
    //Now, we only want NVEncC tabs in the init.
    nvencc_tab = new QList<QStringList>(); //.at(0) = index / .at(1) = name
    nvencc_tab->append(QStringList() << "0" << "Video");
    nvencc_tab->append(QStringList() << "1" << "Audio");
    nvencc_tab->append(QStringList() << "2" << "Other");

    ffmpeg_tab = new QList<QStringList>(); //.at(0) = index / .at(1) = name
    ffmpeg_tab->append(QStringList() << "3" << "Video");
    ffmpeg_tab->append(QStringList() << "4" << "Audio/VFilter");
    ffmpeg_tab->append(QStringList() << "2" << "Other"); //Yeah, we use the same `Other` tab as nvencc.

    waifu_tab = new QList<QStringList>(); //.at(0) = index / .at(1) = name
    waifu_tab->append(QStringList() << "5" << "Waifu2x");

    ui->settings_tab->clear();
    foreach (const QStringList &ii, *nvencc_tab)
        ui->settings_tab->addTab(tabList->at(ii.at(0).toInt()), ii.at(1));

    mode = 0; //0 = nvencc / 1 = ffmpeg. GUI-wide / 3 = Waifu2x.
    add_history_n("[ Encoder ]", "Active: \"" + ui->radio_nvencc->text() + "\"");

    timer = new QTimer(this);
    enc_queue = new QQueue<QStringList>();
    QAbstractItemModel* fileListModel = ui->fileList->model();

    last_nvenc_args = QStringList();
    last_process_nvenc = false;

    //INITIALIZE DEFAULT SETTINGS
    settings_list = new QList<Setting*>();
    default_setting = get_setting();
    current_setting = 0;

    //Buttons
    connect(ui->btnAdd, SIGNAL(clicked()), this, SLOT(add_dir()));
    connect(ui->btnAddf, SIGNAL(clicked()), this, SLOT(add_file()));
    connect(ui->btnDel, SIGNAL(clicked()), this, SLOT(del_file_list()));
    connect(ui->encode_btn, SIGNAL(clicked()), this, SLOT(encode()));
    //Menus
    connect(ui->menuPresets, SIGNAL(aboutToShow()), this, SLOT(check_presets()));
    connect(ui->menuFile, SIGNAL(aboutToShow()), this, SLOT(reset_watcher()));
    //Menu actions
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(exit()));
    connect(ui->actionCheck, SIGNAL(triggered()), this, SLOT(check()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionReset_job_queue, SIGNAL(triggered()), this, SLOT(reset_jobq()));
    connect(ui->actionToggle_log, SIGNAL(triggered()), this, SLOT(toggle_log_slot()));
    connect(ui->actionSelect_log_dir, SIGNAL(triggered()), this, SLOT(change_log_dir_slot()));
    connect(ui->actionContact, SIGNAL(triggered()), this, SLOT(contact_slot()));
    connect(ui->actionSubtitle_extractor, SIGNAL(triggered()), this, SLOT(button_sub_slot()));
    connect(ui->actionClear_queue, SIGNAL(triggered()), this, SLOT(abort_slot()));
    connect(ui->actionClear_history, SIGNAL(triggered()), this, SLOT(clear_history_slot()));
    connect(ui->actionShow, SIGNAL(triggered()), this, SLOT(toggle_encoding_queue_slot()));
    connect(ui->actionChange_Base_dir, SIGNAL(triggered()), this, SLOT(change_base_dir_slot()));
    connect(ui->actionManage_job_queue, SIGNAL(triggered()), this, SLOT(manage_db_slot()));
    connect(ui->actionLight_default, SIGNAL(triggered()), this, SLOT(skin_light_slot()));
    connect(ui->actionDark, SIGNAL(triggered()), this, SLOT(skin_dark_slot()));
    connect(ui->actionMedia_splitter_merger, SIGNAL(triggered()), this, SLOT(media_splitter_merger_slot()));
    connect(ui->actionSave_job_queue, SIGNAL(triggered()), this, SLOT(save_job_queue_slot()));
    connect(ui->actionLoad_job_queue, SIGNAL(triggered()), this, SLOT(load_job_queue_slot()));
    //Auxiliar process
    connect(proc, SIGNAL(readyReadStandardError()), this, SLOT(proc_read()));
    connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
    connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));
    //File list
    connect(ui->fileList, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(mkvinfo(int,int)));
    connect(ui->fileList, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(edit_name()));
    connect(ui->fileList, SIGNAL(cellClicked(int,int)), this, SLOT(job_selected_slot(int,int)));
    connect(fileListModel, SIGNAL(rowsRemoved(QModelIndex,int,int)), this, SLOT(rows_watcher()));
    connect(fileListModel, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(rows_watcher()));
    //Subtitle toggle
    connect(ui->check_subs, SIGNAL(stateChanged(int)), this, SLOT(toggle_ui_subs()));
    //Tune toggle
    connect(ui->tuneCheck, SIGNAL(stateChanged(int)), this, SLOT(toggle_tune()));
    //Radio buttons to change mode nvencc/ffmpeg
    connect(ui->radio_nvencc, SIGNAL(toggled(bool)), this, SLOT(radio_encoder_watcher(bool)));
    connect(ui->radio_ffmpeg, SIGNAL(toggled(bool)), this, SLOT(radio_encoder_watcher(bool)));
    connect(ui->radio_waifu2x, SIGNAL(toggled(bool)), this, SLOT(radio_encoder_watcher(bool)));
    //Copy audio/video activate/deactivate
    connect(ui->audioCodec, SIGNAL(currentTextChanged(QString)), this, SLOT(audio_codec_slot(QString)));
    connect(ui->audioCodec_2, SIGNAL(currentTextChanged(QString)), this, SLOT(audio_codec_slot_2(QString)));
    connect(ui->videoCodec, SIGNAL(currentTextChanged(QString)), this, SLOT(video_codec_slot(QString)));
    //More reactive things
    connect(ui->dualAudioCheckbox, SIGNAL(stateChanged(int)), this, SLOT(dual_audio_toggle(int)));
    connect(ui->dualAudioCheckbox_2, SIGNAL(stateChanged(int)), this, SLOT(dual_audio_toggle(int)));
}

MainWindow::~MainWindow(){
    delete ui;
}

// GUI THINGS

void MainWindow::add_dir(){
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Open folder with videos"), basedir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    add_dir_func(fileName);
    auto_select_new_job();
}

void MainWindow::add_dir_func(const QString& fileName){
    bool exists = false;
    if ( !fileName.isNull() ){
        for( int x=0; x<ui->fileList->rowCount(); x++ ){
            if( (basedir + ui->fileList->item(x, 0)->text()) == fileName && !ui->actionAllow_same_file_on_queue_more_than_once->isChecked() ){
                QMessageBox::information(this, tr("Folder already selected"), tr(qPrintable(ui->fileList->item(x, 0)->text() + " is already in the job list.")), QMessageBox::Ok);
                exists = true;
                break;
            }
        }
        if(!exists){
            QString s = fileName;
            s = s.remove(basedir);
            ui->fileList->insertRow( ui->fileList->rowCount() );
            auto *item = new QTableWidgetItem( s );
            QTableWidgetItem *i2 = new QTableWidgetItem ( edit_name_helper(s) );
            ui->fileList->setItem( ui->fileList->rowCount()-1, 0, item );
            ui->fileList->setItem( ui->fileList->rowCount()-1, 1, i2 );
            add_history_n("[ Add ]", "\"" + s + "\"");
            QStringList l = (QStringList()<<s<<"D");
            pathList->append(l); //D for directory
            //Settings code
            settings_list->append(new Setting(get_setting())); //Añade setting actual a la lista para el nuevo trabajo
        }
    }
}

void MainWindow::add_file(){
    QStringList fileName = QFileDialog::getOpenFileNames(this, tr("Open video/s"), basedir, "Video Files (*.mkv *.mp4 *.m4v *.avi *.wmv *.ts)");
    if(!fileName.isEmpty()){
        for(int i=0; i<fileName.count(); i++){
            add_file_func(fileName.at(i));
        }
    }
    auto_select_new_job();
}

void MainWindow::add_file_func(const QString& fileName){
    bool exists = false;
    if ( !fileName.isNull() ){
        for( int x=0; x<ui->fileList->rowCount(); x++ ){
            if( (basedir + ui->fileList->item(x, 0)->text()) == fileName && !ui->actionAllow_same_file_on_queue_more_than_once->isChecked() ){
                QMessageBox::information(this, tr("Video already selected"), tr(qPrintable(ui->fileList->item(x, 0)->text() + " is already in the job list.")), QMessageBox::Ok);
                exists = true;
                break;
            }
        }
        if(!exists){
            QString s = fileName;
            s = s.remove(basedir);
            ui->fileList->insertRow( ui->fileList->rowCount() );
            auto *item = new QTableWidgetItem( s );
            QTableWidgetItem *i2 = new QTableWidgetItem ( edit_name_helper(s) );
            ui->fileList->setItem( ui->fileList->rowCount()-1, 0, item );
            ui->fileList->setItem( ui->fileList->rowCount()-1, 1, i2 );
            add_history_n("[ Add ]", "\"" + s.split("/").last() + "\"");
            QStringList l = (QStringList()<<fileName<<"F");
            pathList->append(l); //F for file
            //Settings code
            settings_list->append(new Setting(get_setting())); //Añade setting actual a la lista para el nuevo trabajo
        }
        exists = false;
    }
}

void MainWindow::edit_name(){
    if(!ui->fileList->selectedItems().isEmpty()){
        bool ok;
        QString name;
        QString default_name;
        int row = ui->fileList->currentRow();
        if(ui->fileList->item(row, 1)->text() == "" || ui->fileList->item(row, 1)->text().isEmpty()){
            default_name = edit_name_helper(ui->fileList->item(row, 0)->text());
            name = QInputDialog::getText(this, tr("New name chooser"), tr("New name: "), QLineEdit::Normal, default_name, &ok);
        } else {
            default_name = ui->fileList->item(row, 1)->text();
            name = QInputDialog::getText(this, tr("New name chooser"), tr("New name: "), QLineEdit::Normal, default_name, &ok);
        }
        if(ok && !name.isEmpty()){
            auto *it = new QTableWidgetItem( name );
            ui->fileList->setItem( row, 1, it );
            add_history_n("[ Name ]", "\"" + ui->fileList->item(row, 0)->text() + "\"->\"" + name + "\"");
        }
    } else
        add_file();
}

QString MainWindow::edit_name_helper(QString text){
    if(text.startsWith("Movies/"))
        return text.replace("Movies/", "").left(text.replace("Movies/", "").indexOf("/")).replace(".mkv", "").replace(".m4v", "").replace(".mp4", "").replace(".wmv", "").replace(".ts", "");
    else if (!text.contains(':')){ //if it contains the : character, it means it includes a full path with the disk name (on windows)
        QString tmp = "";
        QRegExp season_ep_regexp = QRegExp("(?:.+)(S\\d\\dE\\d\\d)"); //searchs for SxxExx in the name of the file
        season_ep_regexp.setCaseSensitivity(Qt::CaseInsensitive);
        season_ep_regexp.indexIn(text);
        tmp.append(text.left(text.indexOf("/")));
        tmp.append("_");
        if(season_ep_regexp.cap(1).isEmpty()){
            tmp.append(text.right(text.count() - text.lastIndexOf("/") - 1));
        } else
            tmp.append(season_ep_regexp.cap(1).toUpper());
        return tmp;
    } else {
        return text.right(text.count() - text.lastIndexOf("/") - 1);
    }
}

void MainWindow::del_file_list(){
    auto *rows_to_remove = new QList<int>();
    for( int x=0; x<ui->fileList->rowCount(); x++ ) { //Mark the rows to remove
        if( ui->fileList->item(x, 0)->isSelected() ){
            add_history_n("[ Remove ]", "\"" + ui->fileList->item(x, 0)->text() + "\"");
            rows_to_remove->append(x);
        }
    }
    for(int jj = rows_to_remove->size()-1; jj >= 0 ; jj--){ //Remove them in the reverse order (to not fuck up with the index)
        ui->fileList->removeRow(rows_to_remove->at(jj));
        pathList->removeAt(rows_to_remove->at(jj));
        //qDebug() << "Removing: " << rows_to_remove->at(jj);
        //Settings code
        //When deleting a job, we should load the previous setting, as well as setting it as selected. Then, we remove the setting from the list.
        settings_list_remove(rows_to_remove->at(jj));
    }
}

void MainWindow::settings_list_remove(int x){
    settings_list->removeAt(x);
    if(settings_list->isEmpty()){
        current_setting = 0;
        set_setting(default_setting);
    } else {
        if(current_setting > 0)
            current_setting--;
        set_setting(settings_list->at(current_setting));
        ui->fileList->selectRow(current_setting);
    }
}

void MainWindow::job_selected_slot(int row, int col){
    if(row != current_setting){
        settings_list->replace(current_setting, get_setting());
        set_setting(settings_list->at(row));
        current_setting = row;
    }
}

void MainWindow::auto_select_new_job(){
    if(ui->fileList->rowCount() != 0){
        ui->fileList->selectRow(ui->fileList->rowCount()-1);
        job_selected_slot(ui->fileList->rowCount()-1, 0);
    }
}

void MainWindow::reset_jobq(){
    add_history_n("[ Reset ]", "Cleaned " + QString::number(ui->fileList->rowCount()) + (ui->fileList->rowCount()==1?" item":" items"));
    ui->fileList->setRowCount(0);
    pathList->clear();
    settings_list->clear();
    current_setting = 0;
}

void MainWindow::save_job_queue_slot(){
    //BINARY SAVE OF SETTINGS
    if(!settings_list->isEmpty()){
        settings_list->replace(current_setting, get_setting()); //guardamos el setting actual antes de guardar
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save jobs"), basedir, "Job Savefile (*.njsf)");
        if(!fileName.isEmpty()){
            QFile out(fileName);
            out.open(QFile::WriteOnly);
            QDataStream str{&out};
            // TODO: pasar qtablewidget a qlist<qstringlist> en norlib.
            //str << *pathList << ui->fileList << *settings_list;
            //QMessageBox::information(this, "Save success", "Success saving to \"" + fileName + "\".");
        }
    }
}

void MainWindow::load_job_queue_slot(){
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open jobs"), basedir, "Job Savefile (*.njsf)");
    if(!fileName.isEmpty()){
        QFile in(fileName);
        in.open(QFile::ReadOnly);
        QDataStream str{&in};
        //TODO
    }
}

void MainWindow::toggle_ui_subs(){
    if(ui->check_subs->isChecked()){
        ui->subChannel->setEnabled(true);
        ui->subSuffix->setEnabled(true);
    } else {
        ui->subChannel->setEnabled(false);
        ui->subSuffix->setEnabled(false);
    }
}

void MainWindow::toggle_tune(){
    if(ui->tuneCheck->isChecked())
        ui->videoTune_2->setEnabled(true);
    else
        ui->videoTune_2->setEnabled(false);
}

void MainWindow::toggle_encoding_queue_slot(){
    if(this->height() == 480) {
        this->setFixedHeight(650);
        ui->actionShow->setText("Hide");
    } else {
        this->setFixedHeight(480);
        ui->actionShow->setText("Show");
    }
}

void MainWindow::check_presets(){
    QDir dir("./presets");
    QString presets_path;
    presets_path = dir.absolutePath();
    presets_path = dir.cleanPath(presets_path);
    QDir dir_presets(presets_path);
    dir_presets.setFilter(QDir::Filters(QDir::Files|QDir::NoSymLinks|QDir::NoDotAndDotDot|QDir::Readable));
    QFileInfoList dir_files = dir_presets.entryInfoList();

    ui->menuPresets->clear();

    if(!dir_files.isEmpty()){
        for(int j = 0; j < dir_files.count(); j++){
            QString fname = dir_files.at(j).fileName();
            if(fname.endsWith(".pre")){
                auto *ac = new QAction(fname, this);
                ui->menuPresets->addAction(ac);
                connect(ac, SIGNAL(triggered()), this, SLOT(preset_slot()));
                //qDebug() << preset_action_list->last()->iconText();
            }
        }
    }
    if(ui->menuPresets->isEmpty())
        ui->menuPresets->addAction(new QAction("No presets found on 'preset/'", this));
}

// IMPORTANT THINGS

void MainWindow::mkvscreen(QStringList argz, int flagg){
    //qDebug() << argz.at(1) << "  " << argz.at(2);
    if(flagg==0){//0=Transcode nvencc
        argz.append("N");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("NVEncC(v)"));
        QString name_table_widget = QString(argz.at(6)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else if (flagg==1){ //1=Sub extraction
        argz.append("S");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("Subtitles"));
        QString name_table_widget = QString(argz.at(4)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else if(flagg==2){ //2=Transcode ffmpeg video
        argz.append("Fv");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("FFmpeg(v/a)"));
        QString name_table_widget = QString(argz.at(4)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else if(flagg==3){ //3=Delete
        argz.append("D");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("Delete"));
        QString name_table_widget = QString(argz.at(0)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else if(flagg==4){ //4=Autoclean
        argz.append("A");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("Clean"));
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(*ui->queue_view->item(0, ui->queue_view->columnCount()-2)));
    } else if(flagg==5){
        argz.append("Dn"); //delete without history
    } else if(flagg==6){
        argz.append("Fn"); //ffmpeg without history
    } else if(flagg==7){ //7=Transcode ffmpeg audio
        argz.append("Fa");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("FFmpeg(a)"));
        QString name_table_widget = QString(argz.at(4)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else if(flagg==8){ //8=ffmpeg merge
        argz.append("Fnq");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("FFmpeg(m)"));
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(*ui->queue_view->item(0, ui->queue_view->columnCount()-2)));
    } else if(flagg==9){ //7=Transcode ffmpeg audio
        argz.append("Fa");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("FFmpeg(dual_a)"));
        QString name_table_widget = QString(argz.at(4)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else if(flagg==10){ //2=Transcode ffmpeg video
        argz.append("Fv");
        ui->queue_view->setColumnCount(ui->queue_view->columnCount()+1);
        ui->queue_view->setHorizontalHeaderItem(ui->queue_view->columnCount()-1, new QTableWidgetItem("FFmpeg(dual_a)"));
        QString name_table_widget = QString(argz.at(6)).remove(basedir);
        int i = name_table_widget.lastIndexOf("/");
        ui->queue_view->setItem(0, ui->queue_view->columnCount()-1, new QTableWidgetItem(name_table_widget.remove(0,i+1)));
    } else return;
    enc_queue->enqueue(argz);
    if(!ui->actionClear_queue->isEnabled())
        ui->actionClear_queue->setEnabled(true);
    //qDebug() <<"ENQUEUE: " << argz;
    if (!timer->isActive())
        timer->singleShot(1000, this, SLOT(queue_tick()) );
}

void MainWindow::queue_tick(){
    if(proc->processId() == 0){ //if process is not running
        if(!enc_queue->isEmpty()){
            if(this->height() == 480){
                this->setFixedHeight(650);
                ui->actionShow->setText("Hide");
            }
            QStringList argz = enc_queue->dequeue();
            //qDebug() << "DEQUEUE: " << argz;
            QString flagg = argz.at(argz.size()-1);
            argz.pop_back();
            mkviUI = new MKVi(this, proc);
            if(flagg=="N"){//N=Transcode nvencc
                error_flag_changable = true;
                last_process_nvenc = true;
                last_nvenc_args = argz;
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                QString name_tmp = QString(argz.at(6)).remove(basedir);
                int i = name_tmp.lastIndexOf("/");
                mkviUI->showios(name_tmp.remove(0,i+1), 1);
                add_history("[ Video NVEnc ]", "\"" + name_tmp + "\"");
            } else if(flagg=="S"){//S=Sub extraction
                error_flag_changable = false;
                last_process_nvenc = false;
                //mkviUI->showios(argz.at(4), 2);
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                QString name_tmp = QString(argz.at(4)).remove(basedir);
                int i = name_tmp.lastIndexOf("/");
                add_history("[ Subtitles ]", "\"" + name_tmp.remove(0,i+1) + "\"");
            } else if(flagg=="Fv"){//Fv=Transcode ffmpeg video
                error_flag_changable = true;
                last_process_nvenc = false;
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                QString name_tmp = QString(argz.at(4)).remove(basedir);
                int i = name_tmp.lastIndexOf("/");
                mkviUI->showios(name_tmp.remove(0,i+1), 1);
                add_history("[ Video FFmpeg ]", "\"" + name_tmp + "\"");
            } else if(flagg=="Fa"){//Fa=Transcode ffmpeg audio
                error_flag_changable = true;
                last_process_nvenc = false;
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                QString name_tmp = QString(argz.at(4)).remove(basedir);
                int i = name_tmp.lastIndexOf("/");
                mkviUI->showios(name_tmp.remove(0,i+1), 1);
                add_history("[ Audio FFmpeg ]", "\"" + name_tmp + "\"");
            } else if(flagg=="D"){//D=delete
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                del_file(argz.first(), true);
                timer->singleShot(1000, this, SLOT(queue_tick()));
                return;
            } else if(flagg=="A"){//A=Autoclean when finished
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                add_history("[ Clean ]", "\"" + ui->fileList->itemAt(0,0)->text() + "\"");
                ui->fileList->removeRow(0);
                pathList->removeAt(0);
                //Settings code
                settings_list_remove(0);
                timer->singleShot(1000, this, SLOT(queue_tick()));
                return;
            } else if(flagg=="Dn"){
                del_file(argz.first(), false);
                timer->singleShot(1000, this, SLOT(queue_tick()));
                return;
            } else if(flagg=="Fn"){
                error_flag_changable = true;
                last_process_nvenc = false;
                timer->singleShot(1000, this, SLOT(queue_tick()));
            } else if(flagg=="Fnq"){//Fnq = ffmpeg no-history queue (used by merge right now)
                error_flag_changable = true;
                last_process_nvenc = false;
                //qDebug() << "REMOVED_COLUMN: " << ui->queue_view->horizontalHeaderItem(0)->text();
                ui->which->setText(ui->queue_view->item(0, 0)->text());
                ui->what->setText(ui->queue_view->horizontalHeaderItem(0)->text());
                ui->queue_view->removeColumn(0);
                timer->singleShot(1000, this, SLOT(queue_tick()));
            } else return;
            proc->start("cmd.exe", argz);
        } else {
            if(this->height() == 650){
                this->setFixedHeight(480);
                ui->actionShow->setText("Show");
            }
            ui->which->setText("");
            ui->what->setText("");
            //qDebug() << "Enabling bttnz";
            rows_watcher();
        }
    } else //if the process is already running, call the tick again
        timer->singleShot(1000, this, SLOT(queue_tick()));
}

void MainWindow::mkvinfo(int x, int y){
    QDir dir(".");
    QString mediainfo_path;
    mediainfo_path = dir.absoluteFilePath("mediainfo/mediainfo.exe");
    mediainfo_path = dir.cleanPath(mediainfo_path);
    if(dir.exists(mediainfo_path)){ //Si existe mediainfo, lo usamos, sino, avisamos.
        QStringList arguments = QStringList() << "/C" << mediainfo_path << basedir + ui->fileList->item(x, 0)->text();
        mkvinfoscreen(arguments);
    } else
        QMessageBox::information(this, tr("MediaInfo not found."), tr("MediaInfo not found.\nIf you want to see media information from the job list, please, follow the instructions in the git."), QMessageBox::Ok);
}

void MainWindow::mkvinfoscreen(const QStringList& argz){
    mkviUI = new MKVi(this, proc);
    mkviUI->showios(argz.at(2), 0);
    proc->start("cmd.exe", argz);
}

void MainWindow::encode(){
    int x = 0;
    int aux_count = pathList->count();
    //Antes de nada, guardamos el setting actual.
    if(!pathList->isEmpty())
        settings_list->replace(current_setting, get_setting());
    else
        ui->encode_btn->setEnabled(false);

    for(int xi = 0; xi < aux_count; xi++){
        QString t = pathList->at(x).at(1);
        QString p = pathList->at(x).at(0);
        //Cargamos cada setting antes de poner nada en la cola, para que cada job tenga sus propios settings.
        set_setting(settings_list->at(x));
        if(!ui->fileList->item(x, 1)->text().isEmpty()){
            if(t=="F"){
                //qDebug() << "File: " << ui->fileList->item(x, 0)->text();
                if(mode == 0 || mode == 1){ //ffmpeg / nvenc
                    if(mode == 0)
                        enc_file_nvencc(p, ui->fileList->item(x,1)->text());
                    else if(mode == 1)
                        enc_file_ffmpeg(p, ui->fileList->item(x,1)->text());
                    if(ui->check_subs->isChecked())
                        extract_sub(p, ui->fileList->item(x,1)->text());
                    if(ui->delFinished->isChecked())
                        mkvscreen(QStringList() << p, 3);
                    if(ui->actionAuto_clean_job_list->isChecked())
                        mkvscreen(QStringList(), 4);
                } else if(mode == 2){ // waifu2x
                    // first, we extract the audio
//                    int index_1 = p.lastIndexOf("/");
//                    QString save_path = p.left(index_1);
//                    QString file_n = p.right(p.count()-(index_1+1));
//                    save_path.append("/");
//                    auto split_aux = new MediaSplitterMerger(this, save_path);
//                    split_aux->hide();
//                    split_aux->split_file(p, save_path+file_n+".vid.mkv", save_path+file_n+".aud.mkv");
                    // now we got the video in "file_n".vid.mkv and the audio in "file_n".aud.mkv.
                    // the next step is extracting the frames of the extracted video in a folder. we need the fps for that.
                }
                //qDebug() << "Disabling bttnz file";
                ui->encode_btn->setEnabled(false);
                ui->btnAdd->setEnabled(false);
                ui->btnAddf->setEnabled(false);
                ui->btnDel->setEnabled(false);
            } else if(t=="D"){
                //qDebug() << "encode - directory: " << p;
                QDir dir_path(basedir+p);
                dir_path.setFilter(QDir::Filters(QDir::Files|QDir::NoSymLinks|QDir::NoDotAndDotDot|QDir::Readable));
                QFileInfoList dir_files = dir_path.entryInfoList();
                QStringList enc_files;
                bool atleastone = false;
                if(!dir_files.isEmpty()){
                    for(int j = 0; j < dir_files.count(); j++){
                        QString fname_tmp = dir_files.at(j).fileName();
                        if(fname_tmp.endsWith(".mkv") || fname_tmp.endsWith(".mp4") || fname_tmp.endsWith(".m4v") || fname_tmp.endsWith(".avi") || fname_tmp.endsWith(".wmv") || fname_tmp.endsWith(".ts"))
                            enc_files.append(fname_tmp);
                    }
                    for(int j = 0; j < enc_files.count(); j++){
                        const QString& fname = enc_files.at(j);
                        atleastone = true;
                        //qDebug() << fname;
                        QString out_prefix = ui->outputPrefix->text();
                        int i = out_prefix.indexOf("$s");
                        if(i!=-1)
                            out_prefix = out_prefix.replace(i, 2, format_number(ui->seasonN->value(), 2));
                        i = out_prefix.indexOf("$e2");
                        if(i!=-1)
                            out_prefix = out_prefix.replace(i, 3, format_number(j + ui->firstEp->value(), 2));
                        i = out_prefix.indexOf("$e1");
                        if(i!=-1)
                            out_prefix = out_prefix.replace(i, 3, QString::number(j + ui->firstEp->value()));

                        QString tm = ui->fileList->item(x,1)->text();
                        tm.append(out_prefix);
                        if(mode == 0 || mode == 1){ //ffmpeg / nvenc
                            if(mode == 0)
                                enc_file_nvencc(basedir + p + "/" + fname, tm);
                            else if(mode == 1)
                                enc_file_ffmpeg(basedir + p + "/" + fname, tm);
                            if(ui->check_subs->isChecked())
                                extract_sub(basedir + p + "/" + fname, tm);
                            if(ui->delFinished->isChecked())
                                mkvscreen(QStringList() << basedir + p + "/" + fname, 3);
                        } else if(mode == 2){ // waifu2x

                        }
                    }
                    if(!atleastone){
                        QMessageBox::information(this, tr("Folder without transcodable files."), tr(qPrintable("There are no transcodable files in \"" + p + "\"")), QMessageBox::Ok);
                        pathList->removeAt(x);
                        //Settings code
                        settings_list_remove(x);
                        add_history_n("[ ERROR ]", "Directory \"" + p + "\" has no transcodable files");
                        ui->fileList->removeRow(x);
                        x--;
                    } else if(ui->actionAuto_clean_job_list->isChecked())
                        mkvscreen(QStringList(), 4);
                    //qDebug() << "Disabling bttnz dir";
                    ui->encode_btn->setEnabled(false);
                    ui->btnAdd->setEnabled(false);
                    ui->btnAddf->setEnabled(false);
                    ui->btnDel->setEnabled(false);
                } else {
                    QMessageBox::information(this, tr("Folder without files."), tr(qPrintable("There are no files in \"" + p + "\"")), QMessageBox::Ok);
                    pathList->removeAt(x);
                    //Settings code
                    settings_list_remove(x);
                    add_history_n("[ ERROR ]", "Directory \"" + p + "\" has no files. Removed from queue.");
                    ui->fileList->removeRow(x);
                    x--;
                }
            }
        } else {
            QMessageBox::information(this, tr("Name missing"), qPrintable(p + " had no assigned name. Got removed."), QMessageBox::Ok);
            pathList->removeAt(x);
            //Settings code
            settings_list_remove(x);
            ui->fileList->removeRow(x);
            x--;
            add_history_n("[ ERROR ]", "Directory \"" + p + "\" had no name. Removed from queue.");
        }
        x++;
    }
    set_setting(settings_list->at(current_setting));
}

void MainWindow::enc_file_nvencc(const QString& p, const QString& outname){
    QDir dir(".");
    QString nvencc_path;
    nvencc_path = dir.absoluteFilePath("nvencc/NVEncC64.exe");
    nvencc_path = dir.cleanPath(nvencc_path);
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    if(dir.exists(nvencc_path)){ //Si existe nvencc, lo usamos, sino, avisamos.
        if(dir.exists(ffmpeg_path)){ //Si existe ffmpeg, lo usamos, sino, avisamos.
            int index_1 = p.lastIndexOf("/");
            QString save_path = p.left(index_1);
            bool no_audio = false;
            bool no_video = false;
            bool copy_vid = false;
            bool copy_aud = false;
            bool dual_aud = false;
            save_path.append("/");
            QStringList arguments_video;
            QStringList arguments_audio;

            //OPTIONS
            if(ui->videoCodec->currentText()=="copy")
                copy_vid = true;
            if(ui->audioCodec->currentText()=="copy")
                copy_aud = true;
            if(ui->audioCodec->currentText() == "no-audio")
                no_audio = true;
            if(ui->videoCodec->currentText() == "no-video")
                no_video = true;
            if(ui->dualAudioCheckbox->isChecked())
                dual_aud = true;

            // AUDIO
            if(!copy_aud && !no_audio)
                    arguments_audio = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << p << "-vn" << "-map" << "0:a:"+QString::number(ui->audioChannel->value()) << "-c:a" << ui->audioCodec->currentText() <<
                                                     "-b:a" << (QString::number(ui->audioBitrate->value()) + "k") << (save_path + (no_video?outname:"[audio]"+outname) + ".mp4");
            else if(copy_aud)
                arguments_audio = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << p << "-vn" << "-map" << "0:a:"+QString::number(ui->audioChannel->value()) << "-c:a" <<
                                                   ui->audioCodec->currentText() << (save_path + (no_video?outname:"[audio]"+outname) + ".mp4");

            // VIDEO
            if(!copy_vid && !no_video)
                    arguments_video = QStringList() << "/C" << nvencc_path <<
                                                     "--avhw" << "--fps" << QString::number(ui->video_fps->value()) << "-i" << p <<
                                                     "--preset" << "quality" << "--vbrhq" << (QString::number(ui->videoBitrate->value())+"k") << "--vbr-quality" << "14" <<
                                                     "--qp-init" << "14:14:14" << "--lookahead" << "3" << ((ui->AQtemporal->isChecked())?"--aq-temporal":"") <<
                                                     ((ui->AQspatial->isChecked())?"--aq":"") << ((ui->AQspatial->isChecked() || ui->AQtemporal->isChecked())?"--aq-strength":"") <<
                                                     ((ui->AQspatial->isChecked() || ui->AQtemporal->isChecked())?"12":"") << "--profile" << "high" << "--level" << "4" <<
                                                     "--codec" << ui->videoCodec->currentText() << "--avsync" << "forcecfr" << "--output-thread" << "1" <<
                                                    "--max-bitrate" << "8000" << "--mv-precision" << "Q-pel" << "-o" << save_path + "[nvenc]"+outname + ".mkv";
            else if (copy_vid)
                arguments_video = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << p << "-an" << "-map" << "0:v:"+QString::number(ui->videoChannel->value()) << "-c:v" <<
                                                   ui->videoCodec->currentText() << (save_path + (no_audio?outname:"[video]"+outname) + ".mp4");

            // INVOKE

            // clean
            arguments_video.removeAll("");
            arguments_audio.removeAll("");

            if(copy_vid) // copy_video_ffmpeg
                mkvscreen(arguments_video, 2);
            else if(!no_video){ // transcode_video_nvenc(1) -> copy_video_ffmpeg(1->2) -> delete_(1)
                mkvscreen(arguments_video, 0);
                mkvscreen(QStringList() << "/C" << ffmpeg_path << "-i" << save_path + "[nvenc]"+outname + ".mkv" << "-an" << "-c:v" << "copy" << save_path + (no_audio?outname:"[video]"+outname) + ".mp4", 6);
                mkvscreen(QStringList() << save_path + "[nvenc]"+outname + ".mkv", 5);
            }

            if(!no_audio){ // copy_audio / transcode_audio
                mkvscreen(arguments_audio, 7);
                if(dual_aud){
                    QStringList aux = arguments_audio;
                    aux.last().replace(outname, outname + ui->dual_suffix->text());
                    aux[7] = "0"+QString::number(ui->dualAudioChannel->value());
                    mkvscreen(aux, 9);
                }
            }

            if(!no_audio && !no_video){ // merge_audio+video -> delete [audio] and [video] originals
                mkvscreen(QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << (save_path + "[video]" + outname + ".mp4") << "-i" << (save_path + "[audio]" + outname + ".mp4") << "-c:v" << "copy" << "-c:a" << "copy" << "-movflags" << "faststart" << (save_path + outname + ".mp4"), 8);
                if(dual_aud)
                    mkvscreen(QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << (save_path + "[video]" + outname + ".mp4") << "-i" << (save_path + "[audio]" + outname + ui->dual_suffix->text() + ".mp4") << "-c:v" << "copy" << "-c:a" << "copy" << "-movflags" << "faststart" << (save_path + outname + ui->dual_suffix->text() + ".mp4"), 8);
                if(ui->delete_video_file_checkbox->isChecked())
                    mkvscreen(QStringList() << (save_path + "[video]" + outname + ".mp4"), 5);
                mkvscreen(QStringList() << (save_path + "[audio]" + outname + ".mp4"), 5);
                if(dual_aud)
                    mkvscreen(QStringList() << (save_path + "[audio]" + outname + ui->dual_suffix->text() + ".mp4"), 5);
            }
        } else
            QMessageBox::information(this, tr("Ffmpeg not found."), tr("Ffmpeg not found.\nIf you want to extract subtitles or use ffmpeg, please, follow the instructions in the git."), QMessageBox::Ok);
    } else
        QMessageBox::information(this, tr("NVEncC not found."), tr("NVEncC not found.\nIf you want to transcode with nVidia's GPU-accelerated software, please, follow the instructions in the git."), QMessageBox::Ok);
}

void MainWindow::enc_file_ffmpeg(const QString& p, const QString& outname){
    QDir dir(".");
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);

    bool copy_vid = false;

    //OPTIONS
    if(ui->videoCodec_2->currentText()=="copy")
        copy_vid = true;

    if(dir.exists(ffmpeg_path)){ //Si existe ffmpeg, lo usamos, sino, avisamos.
        bool copy_aud = false;
        int index_1 = p.lastIndexOf("/");
        QString save_path = p.left(index_1);
        save_path.append("/");
        //qDebug() << save_path;
        QStringList argz;
        if(copy_vid)
            argz = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << p << "-map" << "0:v:"+QString::number(ui->videoChannel->value()) << "-map" << "0:a:"+ui->audioChannel_2->text() << "-c:v" << ui->videoCodec_2->currentText() <<
                                                "-c:a" << ui->audioCodec_2->currentText() << (copy_aud?"":"-b:a") <<
                                                (copy_aud?"":(QString::number(ui->audioBitrate_2->value()) + "k")) << "-movflags" << "faststart" << (save_path + outname + ".mp4");
        else
            argz = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << p << "-map" << "0:v:"+QString::number(ui->videoChannel->value()) << "-map" << "0:a:"+ui->audioChannel_2->text() << "-c:v" << ui->videoCodec_2->currentText() << "-crf" << QString::number(ui->videoCRF_2->value()) <<
                                            (ui->videoFilters_2->toPlainText().isEmpty()?"":"-filter:v") <<
                                            (ui->videoFilters_2->toPlainText().isEmpty()?"":ui->videoFilters_2->toPlainText()) <<
                                             "-maxrate" << (QString::number(ui->videoMaxBitrate_2->value()) + "k") << "-bufsize" <<
                                            (QString::number(ui->videoBuffSize_2->value()) + "M") << (ui->tuneCheck->isChecked()?"-tune":"") <<
                                            (ui->tuneCheck->isChecked()?ui->videoTune_2->currentText():"") << "-preset" << ui->videoPreset_2->currentText() <<
                                            "-pix_fmt" << "yuv420p" << "-c:a" << ui->audioCodec_2->currentText() << (copy_aud?"":"-b:a") <<
                                            (copy_aud?"":(QString::number(ui->audioBitrate_2->value()) + "k")) << "-movflags" << "faststart" << "-max_muxing_queue_size" <<
                                            "9999" << (save_path + outname + ".mp4");
        argz.removeAll("");
        //foreach(const QString &sss, argz)
        //    qDebug() << sss;
        mkvscreen(argz, 2);
        if(ui->dualAudioCheckbox_2->isChecked()){
            argz = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << (save_path + outname + ".mp4") << "-i" << p << "-map" << "0:v:0"
                                 << "-map" << "1:a:"+ui->dualAudioChannel_2->text() << "-c:v" << "copy"
                                 << "-c:a" << ui->audioCodec_2->currentText() << (copy_aud?"":"-b:a")
                                 << (copy_aud?"":(QString::number(ui->audioBitrate_2->value()) + "k")) << "-movflags" << "faststart" << (save_path + outname + ui->dual_suffix_2->text() + ".mp4");
            argz.removeAll("");
            mkvscreen(argz, 10);
        }
    } else
        QMessageBox::information(this, tr("Ffmpeg not found."), tr("Ffmpeg not found.\nIf you want to extract subtitles or use ffmpeg, please, follow the instructions in the git."), QMessageBox::Ok);


}

void MainWindow::extract_sub(const QString& fpath, const QString& out_name){
    QDir dir(".");
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    if(dir.exists(ffmpeg_path)){ //Si existe mediainfo, lo usamos, sino, avisamos.
        QStringList arguments = QStringList() << "/C" << ffmpeg_path << "-y" << "-i" << fpath << "-map" << "0:"+QString::number(ui->subChannel->value()) << fpath.section("/", 0, -2) + "/" + out_name + ui->subSuffix->text();
        //qDebug() << arguments.at(4);
        mkvscreen(arguments, 1);
    } else
        QMessageBox::information(this, tr("Ffmpeg not found."), tr("Ffmpeg not found.\nIf you want to extract subtitles or use ffmpeg, please, follow the instructions in the git."), QMessageBox::Ok);
}

void MainWindow::del_file(const QString& path, bool v){
    QDir dir;
    if(!error_flag){
        dir.remove(path);
        if(v){
            QString name_tmp = QString(path).remove(basedir);
            int i = name_tmp.lastIndexOf("/");
            add_history_n("[ Delete ]", "\"" + name_tmp.remove(0,i+1) + "\"");
        }
    } else {
        if(v)
            error_flag = false;
    }
}

// SLOT THINGS

void MainWindow::radio_encoder_watcher(bool t){
    //qDebug() << "NVEncC radio button state: " << t;
    if(t){
        if(sender()->objectName()=="radio_nvencc" && mode != 0){
            ui->settings_tab->clear();
            foreach (const QStringList &ii, *nvencc_tab)
                ui->settings_tab->addTab(tabList->at(ii.at(0).toInt()), ii.at(1));
            mode = 0;
            add_history_n("[ Encoder ]", "Active: \"" + ui->radio_nvencc->text() + "\"");

        } else if(sender()->objectName()=="radio_ffmpeg" && mode != 1) {
            ui->settings_tab->clear();
            foreach (const QStringList &ii, *ffmpeg_tab)
                ui->settings_tab->addTab(tabList->at(ii.at(0).toInt()), ii.at(1));
            mode = 1;
            add_history_n("[ Encoder ]", "Active: \"" + ui->radio_ffmpeg->text() + "\"");
        } else if(sender()->objectName()=="radio_waifu2x" && mode != 2){
            ui->settings_tab->clear();
            foreach (const QStringList &ii, *waifu_tab)
                ui->settings_tab->addTab(tabList->at(ii.at(0).toInt()), ii.at(1));
            mode = 2;
            add_history_n("[ Encoder ]", "Active: \"" + ui->radio_waifu2x->text() + "\"");
        }
    }
}

void MainWindow::change_base_dir_slot(){
    QString newbasedir = QFileDialog::getExistingDirectory(this, "New Base dir");
    if(!newbasedir.isEmpty()){
        add_history_n("[ Basedir ]", "New Base dir: \"" + newbasedir + "\"");
        basedir = newbasedir;
        media_splitter_merger->deleteLater();
        media_splitter_merger = new MediaSplitterMerger(this, basedir);
    }
}

void MainWindow::audio_codec_slot(const QString& new_text){
    if(new_text == "copy"){
        ui->audio75->setEnabled(false);
        ui->audioBitrate->setEnabled(false);
        ui->label_31->setEnabled(false);
        ui->audioVolumen->setEnabled(false);

        ui->audioChannel->setEnabled(true);
        ui->dualAudioCheckbox->setEnabled(true);
        if(ui->dualAudioCheckbox->isChecked()){
            ui->dualAudioChannel->setEnabled(true);
            ui->dual_suffix->setEnabled(true);
        }
    } else if(new_text == "no-audio"){
        ui->audio75->setEnabled(false);
        ui->audioBitrate->setEnabled(false);
        ui->label_31->setEnabled(false);
        ui->audioVolumen->setEnabled(false);

        ui->audioChannel->setEnabled(false);
        ui->dualAudioChannel->setEnabled(false);
        ui->dualAudioCheckbox->setEnabled(false);
        ui->dual_suffix->setEnabled(false);
    } else {
        ui->audioBitrate->setEnabled(true);
        ui->audioChannel->setEnabled(true);
        ui->label_31->setEnabled(true);
        ui->dualAudioCheckbox->setEnabled(true);
        if(ui->dualAudioCheckbox->isChecked()){
            ui->dualAudioChannel->setEnabled(true);
            ui->dual_suffix->setEnabled(true);
        }
        //ui->audio75->setEnabled(true);
        //ui->audioVolumen->setEnabled(true);
    }
}

void MainWindow::audio_codec_slot_2(const QString& new_text){
    if(new_text == "copy"){
        ui->audioBitrate_2->setEnabled(false);
        ui->label_30->setEnabled(false);
    } else {
        ui->audioBitrate_2->setEnabled(true);
        ui->label_30->setEnabled(true);
    }
}

void MainWindow::video_codec_slot(const QString& new_text){
    if(new_text == "copy"){
        ui->videoBitrate->setEnabled(false);
        ui->label_32->setEnabled(false);
        ui->AQspatial->setEnabled(false);
        ui->AQstrength->setEnabled(false);
        ui->AQtemporal->setEnabled(false);
        ui->video_fps->setEnabled(false);

        ui->delete_video_file_checkbox->setEnabled(false);
        ui->videoChannel->setEnabled(true);
    } else if(new_text == "no-video"){
        ui->videoBitrate->setEnabled(false);
        ui->label_32->setEnabled(false);
        ui->AQspatial->setEnabled(false);
        ui->AQstrength->setEnabled(false);
        ui->AQtemporal->setEnabled(false);
        ui->video_fps->setEnabled(false);

        ui->delete_video_file_checkbox->setEnabled(false);
        ui->videoChannel->setEnabled(false);
    } else {
        ui->videoBitrate->setEnabled(true);
        ui->label_32->setEnabled(true);
        ui->AQspatial->setEnabled(true);
        ui->AQstrength->setEnabled(true);
        ui->AQtemporal->setEnabled(true);
        ui->video_fps->setEnabled(true);
        ui->delete_video_file_checkbox->setEnabled(true);
        ui->videoChannel->setEnabled(true);
    }
}

void MainWindow::preset_slot(){
    auto* a = qobject_cast<QAction*>(sender());
    QDir dir(".");
    QString presets_path;
    presets_path = dir.absoluteFilePath("presets/" + a->iconText());
    presets_path = dir.cleanPath(presets_path);
    QFile preset_file(presets_path);
    bool ok;
    if (preset_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QString t = QString(preset_file.readAll());
        QStringList l = t.split("\n", Qt::SkipEmptyParts);
        QString command = "/C " + l.at(0);
        l.removeFirst();
        mkviUI = new MKVi(this, proc);
        mkviUI->ui->outputMKVi->appendPlainText("Command: " + command + "\n------");
        foreach(const QString &param, l){
            QString param_name = param.split(" ").at(0);
            QString param_default = param.split("<").at(1);
            QStringList param_list = param.split("\"", Qt::SkipEmptyParts);
            const QString& param_desc = param_list.at(1);
            QString value = QInputDialog::getText(this, tr("Preset value setter"), tr(qPrintable("Name: " + param_name + "\nDescription: " + param_desc + "\nSet value: ")), QLineEdit::Normal, param_default, &ok);
            if(ok && !value.isEmpty()){
                mkviUI->ui->outputMKVi->appendPlainText("Param: " + param_name  + "\nDescription: " + param_desc + "\nValue: " + value);
                command.replace(param_name, value);
                //qDebug() << param_values.last();
            } else {
                QMessageBox::information(this, tr("Error in parameter"), tr(qPrintable("Value: ERROR, canceled or name empty.\nTry again!")), QMessageBox::Ok);
                return;
            }
        }
        QString confirmation = QInputDialog::getText(this, tr("Proceed?"), tr(qPrintable("Command:\n\"cmd.exe" + command + "\"\nWrite \"K\" to proceed.")), QLineEdit::Normal, "", &ok);
        QStringList command_list = command.split(" ", Qt::SkipEmptyParts);
        if(ok && (confirmation == "K" || confirmation == "k")){
            //qDebug() << command;
            mkviUI->showios(a->iconText(), 3);
            preset_file.close();
            add_history("[ Preset ]", "\"" + preset_file.fileName().split("/").last() + "\"");

            mkviUI->ui->outputMKVi->appendPlainText("---------\nExecuting: \"cmd.exe" + command + "\"\n---------");
            proc->start("cmd.exe", command_list);
            mkviUI->ui->ok->setEnabled(true);
        } else
            QMessageBox::information(this, tr("Failed to confirm"), tr(qPrintable("Preset execution canceled.\nIf you didn't do this on purpose, simply don't delete the OK in the confirmation dialog.")), QMessageBox::Ok);
    } else {
        QMessageBox::information(this, tr("Preset could not open."), tr(qPrintable("The preset \"" + a->iconText() + "\" could not be read.")), QMessageBox::Ok);
        ui->menuPresets->removeAction(a);
    }
}

void MainWindow::proc_read(){
    // WE WILL ASSUME AT THIS POINT THAT ALL ERROR OUTPUT WILL BE FFMPEG'S AND NVENC'S OUTPUT.
    // THE STANDARD OUTPUT IS THE OUTPUT OF MEDIAINFO MAINLY.
    QString tmpS = proc->readAllStandardOutput();
    QRegExp mediainfo_regex = QRegExp("General.\\n");
    if(!tmpS.isEmpty()){
        int index_mediainfo = tmpS.indexOf(mediainfo_regex); //This captures the General thing in mediainfo output.
        if(index_mediainfo != -1){
            mkviUI->ui->outputMKVi->appendPlainText(tmpS); //for now, cause it is not working, and it's not a priority, i'll let it work as it was not parsed.
            //QString plain = mkviUI->ui->outputMKVi->toPlainText();
            //CONTINUE THIS
            return;
        }
        mkviUI->ui->outputMKVi->appendPlainText(tmpS);
    }
    QString tmp = proc->readAllStandardError();
    QRegExp nvencc_regex = QRegExp("\\[\\d{1,3}.\\d%\\]"); //nvencc
    QRegExp ffm_regex_vid = QRegExp("frame=");
    QRegExp ffm_regex_aud = QRegExp("size=");
    if(!tmp.isEmpty()){
        int index_nvencc = tmp.indexOf(nvencc_regex); //This captures the [xx.x%] thing in NVEncC output.
        int index_ffmpeg_vid = tmp.indexOf(ffm_regex_vid); //This captures the frame= xxxx thing in ffmpeg output.
        int index_ffmpeg_aud = tmp.indexOf(ffm_regex_aud); //This captures the size= xxxx thing in ffmpeg output.
        int index_last_n;
        QString plain = mkviUI->ui->outputMKVi->toPlainText();
        if (index_nvencc != -1)
        { //Caso de linea parseable
            index_last_n = plain.indexOf(nvencc_regex);
        }
        else if(index_ffmpeg_vid != -1) //Caso de linea parseable
            index_last_n = plain.indexOf(ffm_regex_vid);
        else if(index_ffmpeg_aud != -1) //Caso de linea parseable
            index_last_n = plain.indexOf(ffm_regex_aud);
        else
        { //Caso de linea no parseable
            mkviUI->ui->outputMKVi->appendPlainText(tmp);
            return;
        }
        //index_last_n = plain.indexOf("\n", -index_last_n);
        plain = plain.left(index_last_n);
        plain.append(tmp);
        mkviUI->ui->outputMKVi->setPlainText(plain); //-1 to get rid of the final \n.
    }
    //qDebug() << "Standard: " << proc->readAllStandardOutput();  // read normal output
    //qDebug() << "Error: " << proc->readAllStandardError();  // read error channel
}

void MainWindow::end_proc(int e){
    mkviUI->ui->ok->setEnabled(true);
    mkviUI->ui->abort->setEnabled(false);
    mkviUI->ui->pause->setEnabled(false);
    if(enc_queue->empty())
        ui->actionClear_queue->setEnabled(false);
    //Here we reset the process AND its connections.
    proc = new QProcess(this);
    connect(proc, SIGNAL(readyReadStandardError()), this, SLOT(proc_read()));
    connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
    connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));    
    if(ui->actionAuto_close_encoding_windows->isChecked() && mode==0 && e==1){ //this means nvencc got killed by the abort button.
        mkviUI->ui->ok->click();
    } else if(e!=0) {
        if((int(e) < -1 || int(e) == 3) && last_process_nvenc){
            add_history_n("[ Error ]", "NVEncC seems to not have started successfully, retrying...");
            proc->start("cmd.exe", last_nvenc_args);
            mkviUI->ui->ok->setEnabled(false);
            mkviUI->ui->abort->setEnabled(true);
            mkviUI->ui->pause->setEnabled(true);
        } else {
            if(error_flag_changable){
                error_flag = true;
                add_history_n("[ Error ]", "Process exit code: " + QString::number(e));
            } else {
                add_history_n("[ Warning ]", "Process exit code: " + QString::number(e) + ". Probably no subtitle was found.");
            }
        }
    } else if(ui->actionAuto_close_encoding_windows->isChecked() && !mkviUI->windowTitle().startsWith("[I]"))
        mkviUI->ui->ok->click();
}

void MainWindow::rows_watcher(){
    if(ui->fileList->rowCount() == 0){
        ui->btnAdd->setEnabled(true);
        ui->btnAddf->setEnabled(true);
        ui->btnDel->setEnabled(false);
        ui->encode_btn->setEnabled(false);
    } else if(enc_queue->isEmpty()){
        ui->btnDel->setEnabled(true);
        ui->encode_btn->setEnabled(true);
    }
    reset_watcher();
}

void MainWindow::reset_watcher(){
    if(ui->fileList->rowCount() == 0){
        ui->actionReset_job_queue->setEnabled(false);
        ui->actionSave_job_queue->setEnabled(false);
    } else {
        ui->actionReset_job_queue->setEnabled(true);
        ui->actionSave_job_queue->setEnabled(true);
    }
}

void MainWindow::check(){
    QDir dir(".");
    QString nvencc_path;
    nvencc_path = dir.absoluteFilePath("nvencc/NVEncC64.exe");
    nvencc_path = dir.cleanPath(nvencc_path);
    QString ffmpeg_path;
    ffmpeg_path = dir.absoluteFilePath("ffmpeg/ffmpeg.exe");
    ffmpeg_path = dir.cleanPath(ffmpeg_path);
    QString mediainfo_path;
    mediainfo_path = dir.absoluteFilePath("mediainfo/mediainfo.exe");
    mediainfo_path = dir.cleanPath(mediainfo_path);
    QString pskill_path;
    pskill_path = dir.absoluteFilePath("pstools/pskill64.exe");
    pskill_path = dir.cleanPath(pskill_path);
    QString pssuspend_path;
    pssuspend_path = dir.absoluteFilePath("pstools/pssuspend64.exe");
    pssuspend_path = dir.cleanPath(pssuspend_path);
    bool ok = true;
    if(!QFileInfo::exists(nvencc_path) || !QFileInfo(nvencc_path).isFile()){
        QMessageBox::information(this, tr("NVEncC not found."), tr("NVEncC not found.\nIf you want to transcode with nVidia's GPU-accelerated software, please, follow the instructions in the git."), QMessageBox::Ok);
        ok = false;
    }
    if(!QFileInfo::exists(ffmpeg_path) || !QFileInfo(ffmpeg_path).isFile()){
        QMessageBox::information(this, tr("Ffmpeg not found."), tr("Ffmpeg not found.\nIf you want to extract subtitles or use ffmpeg, please, follow the instructions in the git."), QMessageBox::Ok);
        ok = false;
    }
    if(!QFileInfo::exists(mediainfo_path) || !QFileInfo(mediainfo_path).isFile()){
        QMessageBox::information(this, tr("MediaInfo not found."), tr("MediaInfo not found.\nIf you want to see media information from the job list, please, follow the instructions in the git."), QMessageBox::Ok);
        ok = false;
    }
    if(!QFileInfo::exists(pskill_path) || !QFileInfo(pskill_path).isFile() || !QFileInfo::exists(pssuspend_path) || !QFileInfo(pssuspend_path).isFile()){
        QMessageBox::information(this, tr("PsTools not found."), tr("Either PsKill or PsSuspend were not found.\nFollow the instructions in the git."), QMessageBox::Ok);
        ok = false;
    }
    if(ok)
        QMessageBox::information(this, tr("Everything works fine!"), tr("Success!\nAlthough the external programs exist and are well placed, that does not mean they are correct, only they are there, and are readable.\nTo see if the programs really work, you shall use it!"), QMessageBox::Ok);
}

void MainWindow::toggle_log_slot(){
    if(ui->actionToggle_log->isChecked()){
        log_enabled = 1;
        ui->actionToggle_log->setText("On (toggle)");
    } else {
        log_enabled = 0;
        ui->actionToggle_log->setText("Off (toggle)");
    }
}

void MainWindow::change_log_dir_slot(){
    QString new_log = QFileDialog::getSaveFileName(this, tr("Choose log file"), log_file.left(log_file.lastIndexOf("/")), "Log (*.log)");
    if(!new_log.isEmpty())
        log_file = new_log;
}

void MainWindow::abort_slot(){
    if(mode==0){ //for nvencc, we use pskill
        QDir dir(".");
        QString pskill_path;
        pskill_path = dir.absoluteFilePath("pstools/pskill64.exe");
        pskill_path = dir.cleanPath(pskill_path);
        if(!QFileInfo::exists(pskill_path) || !QFileInfo(pskill_path).isFile())
            return;
        QProcess *pskill_proc = new QProcess(this);
        QStringList pskill_args;
        pskill_args << "/C" << pskill_path << "-nobanner" << "-t" << "NVEncC64.exe";
        pskill_proc->execute("cmd.exe", pskill_args);
    } else       //for ffmpeg, this works.
        proc->write("q");
    enc_queue->clear();
    ui->queue_view->clear();
    ui->queue_view->setColumnCount(0);
    add_history_n("[ Aborted ]", (mode==0)?"NVEncC process terminated.":"Ffmpeg process terminated.");
}

void MainWindow::pause_slot(){  //TODO: Hacer que pille el pid en vez del nombre del proceso. Al hacer proc->processId() tenemos el pid de cmd.exe, no del hijo..
    QDir dir(".");
    QString pssuspend_path;
    pssuspend_path = dir.absoluteFilePath("pstools/pssuspend64.exe");
    pssuspend_path = dir.cleanPath(pssuspend_path);
    if(!QFileInfo::exists(pssuspend_path) || !QFileInfo(pssuspend_path).isFile())
        return;
    QProcess *pssuspend_proc = new QProcess(this);
    QStringList pssuspend_args;
    if(process_paused){
        pssuspend_args << "/C" << pssuspend_path << "-nobanner" << "-r" << ((mode==0)?"NVEncC64.exe":"ffmpeg.exe");
        mkviUI->ui->pause->setText("PAUSE");
        add_history_n("[ Resumed ]", (mode==0)?"NVEncC process resumed.":"Ffmpeg process resumed.");
    } else {
        pssuspend_args << "/C" << pssuspend_path << "-nobanner" << ((mode==0)?"NVEncC64.exe":"ffmpeg.exe");
        mkviUI->ui->pause->setText("RESUME");
        add_history_n("[ Paused ]", (mode==0)?"NVEncC process paused.":"Ffmpeg process paused.");
    }
    pssuspend_proc->execute("cmd.exe", pssuspend_args);
    process_paused = !process_paused;
}

void MainWindow::clear_history_slot(){
    ui->outTextView->setRowCount(0);
}

void MainWindow::contact_slot(){
    if(!QDesktopServices::openUrl ( QUrl("https://norpushy.ddns.net/") ))
        add_history_n("[ ERROR ]", "Error opening `https://norpushy.ddns.net/`");
}

void MainWindow::button_sub_slot(){
    auto *treh = new Sub_Extractor(this, basedir);
    treh->show();
}

void MainWindow::about(){
    QMessageBox::information(this, "About", "It started being a simple GUI to automate trancoding by NVEncC, but now it's a full program to manage and automate all video and audio jobs that you may need to do (made in Qt and C++) that uses a freshly-compiled version of ffmpeg (if possible) and NVEncC by rigaya to automate transcoding of entire folders and/or single files (and extraction of subtitles).\n\nIt has a job queue, which accepts directories and files alike.\n\nOptionally, you can download MediaInfo by mkvtoolnix, to show information about the files/folders with a double click right from the job queue.\n\nContact, for ideas or bugs: penrodal@hotmail.com", QMessageBox::Ok);
}

// AUX THINGS

QString MainWindow::format_number(int i, int z){
    QString n = QString::number(i);
    while(n.size() < z)
        n.prepend("0");
    return n;
}

void MainWindow::add_history(const QString& a, const QString& b){
    auto *iii = new QTableWidgetItem( a );
    iii->setTextAlignment(Qt::AlignCenter);
    ui->outTextView->insertRow( 0 );
    ui->outTextView->setItem(0, 0, iii);
    iii = new QTableWidgetItem(b);
    ui->outTextView->setItem(0, 1, iii);
    if(log_enabled == 1){
        //qDebug() << log_file;
        QFile f(log_file);
        if(f.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            f.write((a + "\t" + b + "\n").toUtf8());
            f.close();
        } else
            QMessageBox::information(this, tr("Log file error."), tr("An error has ocurred while trying to append the history to the logfile.\nMake sure the folder exists and it's writable.\nIf you want to select the log save folder, you have the option in \"Options>Log history>Select log dir\".\nIn the other hand, if you want to deactivate the option, you can do it in \"Options>Log history>Toggle\""), QMessageBox::Ok);
    }
}

void MainWindow::add_history_n(const QString& a, const QString& b){
    auto *iii = new QTableWidgetItem( a );
    iii->setTextAlignment(Qt::AlignCenter);
    ui->outTextView->insertRow( 0 );
    ui->outTextView->setItem(0, 0, iii);
    iii = new QTableWidgetItem(b);
    ui->outTextView->setItem(0, 1, iii);
}

void MainWindow::manage_db_slot(){
    QDir dir(".");
    QString db_path;
    db_path = dir.absoluteFilePath("./jobs.db");
    db_path = dir.cleanPath(db_path);

    if(dir.exists(db_path)){
        db_manager = new DB_manager(this, "./jobs.db");
        db_manager->show();
    } else {
        //CREAMOS DB NUEVA.
    }
}

void MainWindow::skin_light_slot(){
    if(ui->actionDark->isChecked())
        setStyleSheet("");
    ui->actionLight_default->setChecked(true);
    ui->actionDark->setChecked(false);
}

void MainWindow::skin_dark_slot(){
    if(ui->actionLight_default->isChecked())
        setStyleSheet(NorLib::style_dark());
    ui->actionLight_default->setChecked(false);
    ui->actionDark->setChecked(true);
}

void MainWindow::media_splitter_merger_slot(){
    media_splitter_merger = new MediaSplitterMerger(this, basedir);
    media_splitter_merger->show();
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    bool accepted = true;
    QList<QUrl> urls = event->mimeData()->urls();
    if (!urls.isEmpty()){
        for(int i = 0; i < urls.count(); i++){
            QString fileName = urls.at(i).toLocalFile();
            if (!fileName.isEmpty())
                if(!(QFileInfo(fileName).isDir() || fileName.endsWith(".mkv", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".mp4", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".m4v", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".avi", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".wmv", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".ts", Qt::CaseSensitivity::CaseInsensitive))){
                    accepted = false;
                    break;
                }
        }
    }
    if(accepted)
        event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls = event->mimeData()->urls();
    if (!urls.isEmpty()){
        for(int i = 0; i < urls.count(); i++){
            QString fileName = urls.at(i).toLocalFile();
            if (!fileName.isEmpty()){
                if(QFileInfo(fileName).isDir()){
                    add_dir_func(fileName);
                }
                if(fileName.endsWith(".mkv", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".mp4", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".m4v", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".avi", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".wmv", Qt::CaseSensitivity::CaseInsensitive) || fileName.endsWith(".ts", Qt::CaseSensitivity::CaseInsensitive)){
                    add_file_func(fileName);
                }
            }
        }
        auto_select_new_job();
    }
}

QString MainWindow::get_basedir(){
    return basedir;
}

//ENCODING SETTINGS CODE
FfmpegVideoSetting* MainWindow::get_ffmpeg_video_setting(){
    int output_codec = ui->videoCodec_2->currentIndex();
    int max_rate = ui->videoMaxBitrate_2->value();
    int buff_size = ui->videoBuffSize_2->value();
    int CRF = ui->videoCRF_2->value();
    int preset = ui->videoPreset_2->currentIndex();
    int tune_selected = ui->videoTune_2->currentIndex();
    bool bool_tune = ui->tuneCheck->isChecked();
    return new FfmpegVideoSetting(output_codec, max_rate, buff_size, CRF, preset, tune_selected, bool_tune);
}

void MainWindow::set_ffmpeg_video_setting(FfmpegVideoSetting *setting){
    ui->videoCodec_2->setCurrentIndex(setting->_0);
    ui->videoMaxBitrate_2->setValue(setting->_1);
    ui->videoBuffSize_2->setValue(setting->_2);
    ui->videoCRF_2->setValue(setting->_3);
    ui->videoPreset_2->setCurrentIndex(setting->_4);
    ui->videoTune_2->setCurrentIndex(setting->_5);
    ui->tuneCheck->setChecked(setting->_6);
}

FfmpegAudioSetting* MainWindow::get_ffmpeg_audio_setting(){
    int output_codec = ui->audioCodec_2->currentIndex();
    int bitrate = ui->audioBitrate_2->value();
    int audio_channel = ui->audioChannel_2->value();
    QString video_filter = ui->videoFilters_2->toPlainText();
    return new FfmpegAudioSetting(output_codec, bitrate, audio_channel, video_filter);
}

void MainWindow::set_ffmpeg_audio_setting(FfmpegAudioSetting *setting){
    ui->audioCodec_2->setCurrentIndex(setting->_0);
    ui->audioBitrate_2->setValue(setting->_1);
    ui->audioChannel_2->setValue(setting->_2);
    ui->videoFilters_2->setPlainText(setting->_3);
}

NvenccVideoSetting* MainWindow::get_nvencc_video_setting(){
    int output_codec = ui->videoCodec->currentIndex();
    int bitrate = ui->videoBitrate->value();
    int video_channel = ui->videoChannel->value();
    int FPS = ui->video_fps->value();
    int aq_strength = ui->AQstrength->value();
    bool aq_temporal = ui->AQtemporal->isChecked();
    bool aq_spatial = ui->AQspatial->isChecked();
    bool delete_temp_video = ui->delete_video_file_checkbox->isChecked();
    return new NvenccVideoSetting(output_codec, bitrate, video_channel, FPS, aq_strength, aq_temporal, aq_spatial, delete_temp_video);
}

void MainWindow::set_nvencc_video_setting(NvenccVideoSetting *setting){
    ui->videoCodec->setCurrentIndex(setting->_0);
    ui->videoBitrate->setValue(setting->_1);
    ui->videoChannel->setValue(setting->_2);
    ui->video_fps->setValue(setting->_3);
    ui->AQstrength->setValue(setting->_4);
    ui->AQtemporal->setChecked(setting->_5);
    ui->AQspatial->setChecked(setting->_6);
    ui->delete_video_file_checkbox->setChecked(setting->_7);
}

NvenccAudioSetting* MainWindow::get_nvencc_audio_setting(){
    int output_codec = ui->audioCodec->currentIndex();
    int bitrate = ui->audioBitrate->value();
    int audio_channel = ui->audioChannel->value();
    return new NvenccAudioSetting(output_codec, bitrate, audio_channel);
}

void MainWindow::set_nvencc_audio_setting(NvenccAudioSetting *setting){
    ui->audioCodec->setCurrentIndex(setting->_0);
    ui->audioBitrate->setValue(setting->_1);
    ui->audioChannel->setValue(setting->_2);
}

OtherSetting* MainWindow::get_other_setting(){
    QString out_prefix = ui->outputPrefix->text();
    int first_episode = ui->firstEp->value();
    bool sub_extraction = ui->check_subs->isChecked();
    int sub_track = ui->subChannel->value();
    QString sub_suffix = ui->subSuffix->text();
    bool delete_on_finish = ui->delFinished->isChecked();
    return new OtherSetting(out_prefix, first_episode, sub_extraction, sub_track, sub_suffix, delete_on_finish);
}

void MainWindow::set_other_setting(OtherSetting *setting){
    ui->outputPrefix->setText(setting->_0);
    ui->firstEp->setValue(setting->_1);
    ui->check_subs->setChecked(setting->_2);
    ui->subChannel->setValue(setting->_3);
    ui->subSuffix->setText(setting->_4);
    ui->delFinished->setChecked(setting->_5);
}

Setting* MainWindow::get_setting(){
    return new Setting(get_ffmpeg_video_setting(), get_ffmpeg_audio_setting(), get_nvencc_video_setting(), get_nvencc_audio_setting(), get_other_setting(), mode);
}

void MainWindow::set_setting(Setting *setting){
    set_ffmpeg_video_setting(&setting->ffmpeg_video_setting);
    set_ffmpeg_audio_setting(&setting->ffmpeg_audio_setting);
    set_nvencc_video_setting(&setting->nvencc_video_setting);
    set_nvencc_audio_setting(&setting->nvencc_audio_setting);
    set_other_setting(&setting->other_setting);
    if(setting->mode == 0)
        ui->radio_nvencc->click();
    else if(setting->mode == 1)
        ui->radio_ffmpeg->click();
    else if(setting->mode == 2)
        ui->radio_waifu2x->click();
}

void MainWindow::dual_audio_toggle(int newstate){
    if(newstate == Qt::Checked){
        if(sender() == ui->dualAudioCheckbox){
            ui->dualAudioChannel->setEnabled(true);
            ui->dual_suffix->setEnabled(true);
        } else if(sender() == ui->dualAudioCheckbox_2) {
            ui->dualAudioChannel_2->setEnabled(true);
            ui->dual_suffix_2->setEnabled(true);
        }
    } else {
        if(sender() == ui->dualAudioCheckbox){
            ui->dualAudioChannel->setEnabled(false);
            ui->dual_suffix->setEnabled(false);
        } else if(sender() == ui->dualAudioCheckbox_2){
            ui->dualAudioChannel_2->setEnabled(false);
            ui->dual_suffix_2->setEnabled(false);
        }
    }
}

void MainWindow::exit(){
    close();
}
