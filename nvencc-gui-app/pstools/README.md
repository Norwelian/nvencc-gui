# PsTools
Download [PsTools](https://docs.microsoft.com/en-us/sysinternals/downloads/pstools) from SysInternals (Microsoft).

Extract it, and copy `pskill64.exe` and `pssuspend64.exe` here.

These two are the only tools we will need from PsTools, as we will be using them to abort the encoding process (pskill) and pause/resume it (pssuspend) at command.