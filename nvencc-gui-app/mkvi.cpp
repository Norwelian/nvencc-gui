#include "mkvi.h"
#include "ui_mkvi.h"

MKVi::MKVi(QWidget *parent, QProcess* proc) :
    QDialog(parent),
    ui(new Ui::MKVi)
{
    max_slider = 0;
    ui->setupUi(this);
    if(parent == nullptr)
        exitt();
    connect(ui->ok, SIGNAL(clicked()), this, SLOT(exitt()));
    connect(ui->abort, SIGNAL(clicked()), parent, SLOT(abort_slot()));
    connect(ui->outputMKVi, SIGNAL(textChanged()), this, SLOT(slider_max()));
    connect(ui->pause, SIGNAL(clicked()), parent, SLOT(pause_slot()));
    parent_proc = proc;
}

MKVi::~MKVi()
{
    delete ui;
}

void MKVi::showios(const QString& title, int sw){
    if(sw == 0) //0=Info
        setWindowTitle("[I]nfo: " + title);
    else if(sw == 1) //1=Transcoding
        setWindowTitle("[T]ranscoding: " + title);
    else if(sw == 2) //2=Subtitle extraction
        setWindowTitle("Extracting [S]ubtitles from: " + title);
    else if(sw == 3) //3=Preset
        setWindowTitle("[P]reset: " + title);
    show();
}

void MKVi::slider_max(){
    QTextCursor c = ui->outputMKVi->textCursor();
    c.movePosition(QTextCursor::End);
    ui->outputMKVi->setTextCursor(c);
    ui->outputMKVi->ensureCursorVisible();
}

void MKVi::exitt(){
    close();
    parent_proc->deleteLater();
    parent_proc = nullptr;
}

void MKVi::reject(){
    if(parent_proc != nullptr && parent_proc->processId() != 0){
        //qDebug() << "process running, calling abort.";
        ui->abort->click();
    }
    deleteLater();
    QDialog::reject();
}
