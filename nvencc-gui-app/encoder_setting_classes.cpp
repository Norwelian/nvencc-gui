#include "encoder_setting_classes.h"

QDataStream &operator>>(QDataStream &in, FfmpegVideoSetting &m){
    int _0, _1, _2, _3, _4, _5;
    bool _6;
    in >> _0 >> _1 >> _2 >> _3 >> _4 >> _5 >> _6;
    m = FfmpegVideoSetting(_0,_1,_2,_3,_4,_5,_6);
    return in;
}
QDataStream &operator<<(QDataStream &out, const FfmpegVideoSetting &m){
    out << m._0 << m._1 << m._2 << m._3 << m._4 << m._5 << m._6;
    return out;
}

QDataStream &operator>>(QDataStream &in, FfmpegAudioSetting &m){
    int _0, _1, _2;
    QString _3;
    in >> _0 >> _1 >> _2 >> _3;
    m = FfmpegAudioSetting(_0,_1,_2,_3);
    return in;
}
QDataStream &operator<<(QDataStream &out, const FfmpegAudioSetting &m){
    out << m._0 << m._1 << m._2 << m._3;
    return out;
}

QDataStream &operator>>(QDataStream &in, NvenccVideoSetting &m){
    int _0, _1, _2, _3, _4;
    bool _5, _6, _7;
    in >> _0 >> _1 >> _2 >> _3 >> _4 >> _5 >> _6 >> _7;
    m = NvenccVideoSetting(_0,_1, 2,_3,_4,_5,_6,_7);
    return in;
}
QDataStream &operator<<(QDataStream &out, const NvenccVideoSetting &m){
    out << m._0 << m._1 << m._2 << m._3 << m._4 << m._5 << m._6;
    return out;
}

QDataStream &operator>>(QDataStream &in, NvenccAudioSetting &m){
    int _0,_1,_2;
    in >> _0 >> _1 >> _2;
    m = NvenccAudioSetting(_0,_1,_2);
    return in;
}
QDataStream &operator<<(QDataStream &out, const NvenccAudioSetting &m){
    out << m._0 << m._1 << m._2;
    return out;
}

QDataStream &operator>>(QDataStream &in, OtherSetting &m){
    int _1,_3;
    QString _0,_4;
    bool _2,_5;
    in >> _0 >> _1 >> _2 >> _3 >> _4 >> _5;
    m = OtherSetting(_0,_1,_2,_3,_4,_5);
    return in;
}
QDataStream &operator<<(QDataStream &out, const OtherSetting &m){
    out << m._0 << m._1 << m._2 << m._3 << m._4 << m._5;
    return out;
}

QDataStream &operator>>(QDataStream &in, Setting &m){
    FfmpegVideoSetting ffmpeg_video_setting;
    FfmpegAudioSetting ffmpeg_audio_setting;
    NvenccVideoSetting nvencc_video_setting;
    NvenccAudioSetting nvencc_audio_setting;
    OtherSetting other_setting;
    int mode;
    in >> ffmpeg_video_setting >> ffmpeg_audio_setting >> nvencc_video_setting >> nvencc_audio_setting >> other_setting >> mode;
    m = Setting(ffmpeg_video_setting, ffmpeg_audio_setting, nvencc_video_setting, nvencc_audio_setting, other_setting, mode);
    return in;
}
QDataStream &operator<<(QDataStream &out, const Setting &m){
    out << m.ffmpeg_video_setting << m.ffmpeg_audio_setting << m.nvencc_video_setting << m.nvencc_audio_setting << m.other_setting << m.mode;
    return out;
}
