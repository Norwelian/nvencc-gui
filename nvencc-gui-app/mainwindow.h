#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QMainWindow>
#include <QProcess>
#include <QMessageBox>
#include <QFileDialog>
#include <QByteArray>
#include <QDebug>
#include <QInputDialog>
#include <QTimer>
#include <QQueue>
#include <QDesktopServices>
#include <QMimeData>

#include "mkvi.h"
#include "ui_mkvi.h"

#include "sub_extractor.h"
#include "ui_sub_extractor.h"

#include "db_manager.h"
#include "ui_db_manager.h"

#include "mediasplittermerger.h"
#include "ui_mediasplittermerger.h"

#include "encoder_setting_classes.h"

#include <norlib.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

private:
    Ui::MainWindow *ui;
    MKVi *mkviUI;
    QString basedir;
    DB_manager *db_manager;
    MediaSplitterMerger *media_splitter_merger;
    QProcess *proc;
    QList<QStringList> *pathList;
    QList<QWidget*> *tabList;
    QList<QStringList> *ffmpeg_tab;
    QList<QStringList> *nvencc_tab;
    QList<QStringList> *waifu_tab;
    QList<Setting*> *settings_list;
    int mode;
    QTimer *timer;
    QQueue<QStringList> *enc_queue;
    bool printable;
    int log_enabled;
    QString log_file;
    bool error_flag;
    bool error_flag_changable;
    bool last_process_nvenc;
    bool process_paused;
    QStringList last_nvenc_args;
    int current_setting;

    Setting *default_setting;

public slots:
    void mkvinfoscreen(const QStringList& argz);
    static QString edit_name_helper(QString text);
    QString get_basedir();

private slots:
    void add_dir();
    void add_dir_func(const QString& fileName);
    void add_file();
    void add_file_func(const QString& fileName);
    void del_file_list();
    void mkvinfo(int, int);
    void encode();
    void enc_file_nvencc(const QString& p, const QString& outname);
    void enc_file_ffmpeg(const QString& p, const QString& outname);
    void rows_watcher();
    void proc_read();
    void end_proc(int e);
    void mkvscreen(QStringList argz, int flagg);
    void queue_tick();
    void edit_name();
    QString format_number(int i, int z);
    void extract_sub(const QString& fpath, const QString& out_name);
    void check();
    void check_presets();
    void about();
    void toggle_ui_subs();
    void toggle_tune();
    void preset_slot();
    void reset_jobq();
    void reset_watcher();
    void del_file(const QString& file_path, bool v);
    void radio_encoder_watcher(bool t);
    void add_history(const QString& a, const QString& b);
    void add_history_n(const QString& a, const QString& b);
    void abort_slot();
    void pause_slot();
    void clear_history_slot();
    void toggle_log_slot();
    void change_log_dir_slot();
    void contact_slot();
    void button_sub_slot();
    void toggle_encoding_queue_slot();
    void audio_codec_slot(const QString& new_text);
    void audio_codec_slot_2(const QString& new_text);
    void video_codec_slot(const QString& new_text);
    void change_base_dir_slot();
    void manage_db_slot();
    void skin_light_slot();
    void skin_dark_slot();
    void media_splitter_merger_slot();
    void exit();
    void job_selected_slot(int row, int col);
    void settings_list_remove(int x);
    void auto_select_new_job();
    void save_job_queue_slot();
    void load_job_queue_slot();

    void dual_audio_toggle(int newstate);

    FfmpegVideoSetting *get_ffmpeg_video_setting();
    void set_ffmpeg_video_setting(FfmpegVideoSetting *setting);
    FfmpegAudioSetting *get_ffmpeg_audio_setting();
    void set_ffmpeg_audio_setting(FfmpegAudioSetting *setting);
    NvenccVideoSetting *get_nvencc_video_setting();
    void set_nvencc_video_setting(NvenccVideoSetting *setting);
    NvenccAudioSetting *get_nvencc_audio_setting();
    void set_nvencc_audio_setting(NvenccAudioSetting *setting);
    OtherSetting *get_other_setting();
    void set_other_setting(OtherSetting *setting);
    Setting *get_setting();
    void set_setting(Setting *setting);
};

#endif // MAINWINDOW_H
