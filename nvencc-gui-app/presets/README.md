# Ffmpeg custom presets
The syntax of the `.pre` files is easy:

#### The first line will be the command to execute.

- Separate arguments by spaces (like in the command line)

- Put `ffmpeg` as the first argument in the line, although you can use it to launch every program your shell / command line supports, so, feel free (you shall put the absolute path of the executables if they are not in `nvencc-gui.exe`'s folder.

#### The rest of lines will be like this: `$param1 "Param1 full name"<defaultvalue`, *IN THE ORDER THEY APPEAR IN THE FIRST LINE, ONE PER LINE*.

- The `"Param1 full name"` part **HAS** to be with the `"`.

- The final part is the default value of the parameter. Simply put `<`after the last `"` of `"Param1 full name"` and wrtie the default value next to it. As is.

#### All parameter identifier in the preset file shall start with `$` (eg. `$param1`).

There are four examples of presets in the git, if you have any doubts.

If you need more help, don't hesitate to contact me!